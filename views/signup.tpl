<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar.tpl'}
        </header>

        <main>
            <div class="container">
                {if isset($msg)}
                    <div class="card red-color-border  center">                       
                        <span>{$msg}</span>               
                    </div>
                {/if}
            </div>
            <div class="row hide-on-med-and-down">
                <div class="main-color-text center col s4 l4 m4">
                    <h2>Inscription</h2>
                    <img class="palm-image center" src="../img/palm.png" alt="marvelous palm" />
                    <div>Gérez vos palmiers en détails où que vous soyez</div>
                </div>
                <div class="col s8 m8 l8 left-align">
                    <div class="card main-color-border log-form">
                        <form action="../php/p_signup.php" method="post">
                            <h3 class="center" >Créer son compte en un clic</h3>
                            <div class="divider"></div>
                            <div class="container main-color-text">
                                <div class="row">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">person</i>
                                        <input name="login" id="login" type="text" class="validate">
                                        <label for="login">Pseudo</label>
                                    </div>
                                        
                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">email</i>
                                        <input name="email" id="email" type="email" class="validate">
                                        <label for="email">Mail</label>
                                    </div>

                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">lock</i>
                                        <input name="pass" id="pass" type="password" class="validate">
                                        <label for="pass">Password</label>
                                    </div>

                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">lock</i>
                                        <input name="cpass" id="cpass" type="password" class="validate">
                                        <label for="cpass">Confirm Password</label>
                                    </div>

                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">accessibility</i>
                                        <input name="referral" id="referral" type="text" class="validate">
                                        <label for="referral">Code parain (pas d'espace des deux cotés)</label>
                                    </div>

                                    <button class="right btn main-color" type="submit">
                                        Inscription
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <div class="hide-on-large-only">
                    <div class="container">
                        <div class="row center">
                            <div class="card main-color-border">
                                <form action="../php/p_signup.php" method="post">
                                    <h3 class="center" >Créer son compte en un clic</h3>
                                    <div class="divider"></div>
                                    <div class="container main-color-text">
                                        <div class="row">
                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">person</i>
                                                <input id="icon_prefix" type="text" class="validate">
                                                <label for="icon_prefix">Pseudo</label>
                                            </div>
                                                
                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">email</i>
                                                <input id="icon_email" type="email" class="validate">
                                                <label for="icon_email">Mail</label>
                                            </div>

                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">lock</i>
                                                <input id="icon_pass" type="password" class="validate">
                                                <label for="icon_pass">Password</label>
                                            </div>

                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">lock</i>
                                                <input id="icon_cpass" type="password" class="validate">
                                                <label for="icon_cpass">Confirm Password</label>
                                            </div>

                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">accessibility</i>
                                                <input name="referral" id="referral" type="text" class="validate">
                                                <label for="referral">Code parain</label>
                                            </div>
                                    
                                            <button class="right btn main-color" type="submit">
                                                Inscription
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </main>
    </body>

    <footer>
    </footer>
</html>