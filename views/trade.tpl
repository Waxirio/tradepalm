<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar_connection.tpl' user=$user}
        </header>

        <main>
            <!-- Error message -->
            <div class="container">
                {if isset($_SESSION['err'])}
                    <div class="card red-color-border center">                       
                        <span>{$_SESSION['err']}</span>               
                    </div>
                {/if}
            </div>

            <div class="container">
                <!-- trades a valider -->
                <h4 class="center">Transactions a valider</h4>
                <div class="row">
                    <!-- Par une autre personne -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                                <h5 class="center">Débits à valider par le receveur</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Refuser</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$trans item=$t}
                                            <!-- si le trade est pas validé -->
                                            <!-- si le trade a pour receveur une autre personne -->
                                            {if $t->getValidate() == 0 && $t->getIdR() != $user->getId()}
                                                <tr>
                                                    <form action="../php/p_finishTrade.php" method="post">
                                                        <input class="hide" value="{$t->getId()}" name="id">
                                                        <td>{$t->getNameG()}</td>
                                                        <td>{$t->getNameR()}</td>
                                                        <td>{$t->getAmountPalm()}</td>
                                                        <td>{$t->getAmountPap()}</td>
                                                        <td>
                                                            <button class="btn main-color-negative" value="cancel" type="submit" name="cancel">
                                                                Refuser
                                                            </button>
                                                        </td>
                                                    </form>
                                                </tr>
                                            {/if}
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Par soi -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                            <h5 class="center">Crédits à valider</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Refuser</th>
                                            <th>Valider</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$trans item=$t}
                                            <!-- si le trade est pas validé -->
                                            <!-- si le trade a pour receveur le user connecté -->
                                            {if $t->getValidate() == 0 && $t->getIdR() == $user->getId()}
                                                <tr>
                                                    <form action="../php/p_finishTrade.php" method="post">
                                                        <input class="hide" value="{$t->getId()}" name="id">
                                                        <td>{$t->getNameG()}</td>
                                                        <td>{$t->getNameR()}</td>
                                                        <td>{$t->getAmountPalm()}</td>
                                                        <td>{$t->getAmountPap()}</td>
                                                        <td>
                                                            <button class="btn main-color-negative" value="cancel" type="submit" name="cancel">
                                                                Refuser
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button class="btn main-color-negative" value="accept" type="submit" name="accept">
                                                                Valider
                                                            </button>
                                                        </td>
                                                    </form>
                                                </tr>
                                            {/if}
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <h4 class="center">Transactions validées</h4>
                <div class="row">
                    <!-- trades validés donneur -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                            <h5 class="center">Vos débits</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Emis le</th>
                                            <th>Validé le</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$trans item=$t}
                                            <!-- si le trade est validé -->
                                            <!-- si le trade a pour donneur le user connecté -->
                                            {if $t->getValidate() == 1 && $t->getIdG() == $user->getId()}
                                                <tr>
                                                    <td>{$t->getNameG()}</td>
                                                    <td>{$t->getNameR()}</td>
                                                    <td>{$t->getAmountPalm()}</td>
                                                    <td>{$t->getAmountPap()}</td>
                                                    <td>{$t->getCreatedAt()}</td>
                                                    <td>{$t->getValidateAt()}</td>
                                                </tr>
                                            {/if}
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- trades validés receveur -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                            <h5 class="center">Vos crédits</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Emis le</th>
                                            <th>Validé le</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        {foreach from=$trans item=$t}
                                            <!-- si le trade est validé -->
                                            <!-- si le trade a pour receveur le user connecté -->
                                            {if $t->getValidate() == 1 && $t->getIdR() == $user->getId()}
                                                <tr>
                                                    <td>{$t->getNameG()}</td>
                                                    <td>{$t->getNameR()}</td>
                                                    <td>{$t->getAmountPalm()}</td>
                                                    <td>{$t->getAmountPap()}</td>
                                                    <td>{$t->getCreatedAt()}</td>
                                                    <td>{$t->getValidateAt()}</td>
                                                </tr>
                                            {/if}
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html>