<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar_connection.tpl' user=$user}
        </header>

        <main>
            <div class="container">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <h4 class="center">Envoyer des palmier à :</h4>
                        <div class="card main-color-border center">
                            <form action="../php/p_send.php" method="post">
                                <div class="card-content">
                                    <div> 
                                        Pseudo: <span class="main-color-bold truncate"> {$userReceiver->getLogin()} </span>
                                    </div>
                                    <div> 
                                        Wallet Id: <span class="main-color-bold truncate"> {$userReceiver->getWalletId()} </span>
                                    </div>
                                    <div>
                                        Palmiers: 
                                        <div class="input-field inline main-color-bold">
                                            <input value="0" name="quantitePalm" id="number_give" type="text" class="validate">
                                            <label for="number_give">Quantité de palmiers</label>
                                        </div>
                                    </div>
                                    <div>
                                        Papillotes: 
                                        <div class="input-field inline main-color-bold">
                                            <input value="0" name="quantitePap" id="number_give" type="text" class="validate">
                                            <label for="number_give">Quantité de papillotes</label>
                                        </div>
                                    </div>
                                    <input class="hide" name="id" value="{$userReceiver->getId()}">
                                    <div>                                    
                                        <button class="btn main-color" type="submit" name="action">
                                            Envoyer les palmiers
                                            <i class="material-icons right">send</i>
                                        </button>        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html>