<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar_connection.tpl' user=$user}
        </header>

        <main>
            <div class="container">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card main-color center">
                            <div class="card-content">
                                <h5>Equivalent Palmier / Papillotes</h5>
                                <div>
                                    <span class="market-value"> 
                                        {$lastValue->getPalm()}
                                    </span>
                                    <span class="market-value"> | <span>
                                    <span class="market-value">
                                        {$lastValue->getPap()}
                                    </span>
                                </div>
                                <div>{$lastValue->getPalm()} palmier.s vaut {$lastValue->getPap()} papillote.s</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- pour le calcul direct /!\ pas la valeur prise pour la transaction -->
                <input id="cPalm" class="hide" value="{$lastValue->getPalm()}">
                <input id="cPap" class="hide" value="{$lastValue->getPap()}">

                <div class="row">
                    <div class="col s12 l6 m6">
                        <div class="card main-color-border">
                            <div class="card-content">
                                <div class="row">
                                    <h5 class="center">Acheter des palmiers</h5>
                                    <form method="post" action="./p_buy.php">
                                        
                                        <button class="right btn main-color" type="submit" name="action">Acheter
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l6 m6">
                        <div class="card main-color-border">
                            <div class="card-content">
                                <div class="row">
                                    <h5 class="center">Vendre des palmiers</h5>
                                    <form method="post" action="./p_sell.php">
                                        <div class="input-field col s12">
                                            <input name="nbPalm" placeholder="Placeholder" id="PalmSellValue" type="text" class="validate">
                                            <label for="nbPalm">Nombre de palmiers</label>
                                        </div>
                                        <span class="main-color-text-bold">Papillotes : </span>
                                        <span id="PapSellValue" class="main-color-text-bold">0</span>
                                        <button class="right btn main-color" type="submit" name="action">Vendre
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                {if isset($trades)}
                    <h5 class="center"> Transactions du marché </h5>
                    <div class="row">
                        <div class="col s12 l12 m12">
                            <div class="card main-color">
                                <div class="card-content">
                                    <div class="row">
                                        <table class="white-text">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Palmiers</th>
                                                    <th>Papillotes</th>
                                                    <th>Effectué le</th>
                                                </tr>
                                            </thead>
                                            {foreach from=$trades item=$trade}
                                                <tbody>
                                                    <tr>
                                                        <td>{$trade->getType()}</td>
                                                        <td>{$trade->getPalm()}</td>
                                                        <td>{$trade->getPap()}</td>
                                                        <td>{$trade->getDate()}</td>
                                                    </tr>
                                                </tbody>    
                                            {/foreach}
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                {/if}
            </div>
        </main>
    </body>

    <footer>
        <script type="text/javascript">
            $('#PalmSellValue').on('keyup', function(){
                $('#PapSellValue').html(
                    String($('#PalmSellValue').val()*$('#cPap').val()/$('#cPalm').val())
                );
            });
        </script>
    </footer>
</html>