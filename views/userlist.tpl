<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar_connection.tpl' user=$user}
        </header>

        <main>
            <div class="container">
                {if isset($err)}
                    <div class="card red-color-border center">                       
                        <span>{$err}</span>               
                    </div>
                {/if}
            </div>
            <!-- Search user -->
            <div class="container">
                <h5 class="center">Cherchez un utilisateur par nom ou par wallet Id</h5>
                <nav class="search-zone">
                    <div class="nav-wrapper main-color search-zone">
                        <form action="./userlist.php" method="post">
                            <div class="input-field search-zone">
                                <input name="search" id="search" type="search" required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                <i class="material-icons">close</i>
                            </div>
                        </form>
                    </div>
                </nav>
            </div>

            {if isset($search)}
                <h6 class="center">Résultat de la recherche pour [{$search}]</h6>

                <!-- Search the right one -->
                {if isset($userLogin) && !empty($userLogin)}
                    <div class="container">
                        <ul class="collection with-header main-color-negative">
                            <li class="collection-header"><h4>Trouvez la bonne personne (pseudo)</h4></li>
                            {foreach from=$userLogin item=$u}
                                {if $u != $user}
                                    <form method="post" action="./send.php">
                                        <input class="hide" name="id" value="{$u->getId()}">
                                        <li class="collection-item">
                                            <div>
                                                <div class="row">
                                                    <div class="col s6 m6 l6 main-color-bold valign-wrapper">
                                                        {$u->getLogin()}
                                                    </div>
                                                    <div class="col s6 m6 l6 valign-wrapper">
                                                        {$u->getWalletId()}
                                                        <button class="right btn main-color-btn " type="submit" name="action">Envoyer
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>    
                                            </div>
                                        </li>
                                    </form>
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                {/if}

                {if isset($userWallet) && !empty($userWallet)}
                    <div class="container">
                        <ul class="collection with-header main-color-negative">
                            <li class="collection-header"><h4>Trouvez la bonne personne (walletId)</h4></li>
                            {foreach from=$userWallet item=$u}
                                {if $u != $user}
                                    <form method="post" action="./send.php">
                                        <input class="hide" name="id" value="{$u->getId()}">
                                        <li class="collection-item">
                                            <div>
                                                <div class="row">
                                                    <div class="col s6 m6 l6 main-color-bold">
                                                        {$u->getLogin()}
                                                    </div>
                                                    <div class="col s6 m6 l6">
                                                        {$u->getWalletId()}
                                                        <button class="right btn main-color" type="submit" name="action">Envoyer
                                                            <i class="material-icons right">send</i>
                                                        </button>  
                                                    </div>
                                                </div>    
                                            </div>
                                        </li>
                                    </form>
                                {/if}
                            {/foreach}
                        </ul>
                    </div>
                {/if}
            {/if}            
        </main>
    </body>

    <footer>
    </footer>
</html>