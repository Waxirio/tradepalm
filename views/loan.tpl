<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar_connection.tpl' user=$user}
        </header>

        <main>
            <div class="container">
                <h5 class="center">Vos dettes de palmiers</h5>
                <div class="row">
                    {foreach from=$debts item=$d}
                        {if $d->getFinishedAt() == NULL}
                            <div class="col s12 m12 l12">
                                <div class="card main-color">
                                    <div class="card-content">
                                        <div class="row">
                                            <h5>Dette: {$d->getCreatedAt()}</h5>
                                            <table>
                                                <thead>
                                                <tr class="main-color">
                                                    <th>Dette Palmiers</th>
                                                    <th>Dette Papillotes</th>
                                                    <th>Payement Palmiers</th>
                                                    <th>Payement Papillotes</th>
                                                    <th>Restant Palmiers</th>
                                                    <th>Restant Papillotes</th>
                                                </tr>
                                                </thead>

                                                <tbody>
                                                    <tr>
                                                        <td class="info">{$d->getDebtPalm()}</td>
                                                        <td class="info">{$d->getDebtPap()}</td>
                                                        <td class="info">{$d->getPayPalm()}</td>
                                                        <td class="info">{$d->getPayPap()}</td>
                                                        <td class="info">{$d->getDebtPalm() - $d->getPayPalm()}</td>
                                                        <td class="info">{$d->getDebtPap() - $d->getPayPap()}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        
                                            <div class="divider"></div>
                                            <div class="container">
                                                <h5>Remboursement</h5>
                                                <form method="post" action="./p_payLoan.php">
                                                    <input name="id" class="hide" value="{$d->getId()}">
                                                    <div class="input-field">
                                                        <input value="0" id="amountLoanPalm_name" name="amountLoanPalm" type="text" class="validate white-text">
                                                        <label for="amountLoanPalm_name">Montant en Palmiers</label>
                                                    </div>

                                                    <div class="input-field">
                                                        <input value="0" id="amountLoanPap_name" name="amountLoanPap" type="text" class="validate white-text">
                                                        <label for="amountLoanPap_name">Montant en Papillotes</label>
                                                    </div>

                                                    <button class="right btn main-color-negative" type="submit" name="action">Rembourser
                                                        <i class="material-icons right">send</i>
                                                    </button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        {/if}
                    {/foreach}
                </div>
                
                <h5 class="center">Nouvel emprunt</h5>
                <!-- EMPRUNT -->
                <div class="row ">
                    <div class="col s12 m12 l12 ">
                        <div class="card main-color-border">
                            <div class="card-content">
                                <div class="container">
                                    <div class="row">
                                        <h5 class="center">Choisir les montants</h5>
                                        <form method="post" action="./p_loan.php">
                                            <div class="input-field">
                                                <input value="0" name="amountPalm" id="amount_palm" type="text" class="validate">
                                                <label for="amount_palm">Montant de palmiers</label>
                                            </div>
                                            <div class="input-field">
                                                <input value="0" name="amountPap" id="amount_pap" type="text" class="validate">
                                                <label for="amount_pap">Montant de papillotes</label>
                                            </div>

                                            <button class="right btn main-color" type="submit" name="action">Emprunter
                                                <i class="material-icons right">send</i>
                                            </button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Emprunts clos -->

                <h5 class="center">Empreints clos</h5>
                {foreach from=$debts item=$d}
                    {if $d->getFinishedAt() != NULL}
                        <div class="col s12 m12 l12">
                            <div class="card main-color">
                                <div class="card-content">
                                    <div class="row">
                                        <h5>Dette:</h5>
                                        <table>
                                            <thead>
                                                <tr class="main-color">
                                                    <th>Debut</th>
                                                    <th>Fin</th>
                                                    <th>Montant en Palmiers</th>
                                                    <th>Montant en Papillotes</th>
                                                </tr>
                                            </thead>

                                            <tbody>
                                                <tr>
                                                    <td class="info">{$d->getCreatedAt()}</td>
                                                    <td class="info">{$d->getFinishedAt()}</td>
                                                    <td class="info">{$d->getDebtPalm()}</td>
                                                    <td class="info">{$d->getDebtPap()}</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    {/if}
                {/foreach}
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html>