<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar_connection.tpl' user=$user}
        </header>

        <main>
            <!-- Image and title on it -->
            <div class="image-container">
                <img style="width:100%;" src="../img/bandeau.png" alt="bandeau">
                <h4 class="title-on-image center main-color-text hide-on-large-only">Dashboard</h4>
                <h2 class="title-on-image center main-color-text hide-on-med-and-down">Dashboard</h2>
            </div>

            <div class="container">
                {if isset($err)}
                    <div class="card red-color-border center">                       
                        <span>{$err}</span>               
                    </div>
                {/if}
            </div>

            <div class="container">
                <!-- Row for palms and account informations -->
                <div class="row">
                    <div class="col s12 l6 m12">
                        <div class="info-card card main-color center">
                            <div class="card-content center">
                                <h5 class="center">Info des palmiers</h5>
                                <div>
                                    <span class="title-info"> Nombre de palmiers :</span>
                                    <span class="info"> {$user->getPalm()} </span>
                                </div>
                                <div>
                                    <span class="title-info"> Palmiers en cours d'envoi : </span>
                                    <span class="info"> {$restPalm} </span>
                                </div>
                                <div>
                                    <span class="title-info"> Wallet Id : </span>
                                    <span class="info-id"> {$user->getWalletId()} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l6 m12">
                        <div class="info-card card main-color center">
                            <div class="card-content center">
                                <h5 class="center">Info du compte</h5>
                                <div>
                                    <span class="title-info"> Pseudo : </span>
                                    <span class="info"> {$user->getLogin()} </span>
                                </div>
                                <div>
                                    <span class="title-info"> Email : </span>
                                    <span class="info"> {$user->getEmail()} </span>
                                </div>
                                <div>
                                    <span class="title-info"> Compte crée le : </span>
                                    <span class="info"> {$user->getCreatedAt()} </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row title -->
                <div class="row">
                    <div class="col s12 m12 l12">
                        <h4 class="center">- Actions -</h4>
                    </div>
                </div>

                <!-- Row for other information -->
                <div class="row">
                    <div class="col s6 m6 l6">
                        <a href="./trade.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Gestionnaire des envois
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="col s6 m6 l6">
                        <a href="./userlist.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Envoyer des palmiers/papillotes
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col s6 m6 l6">
                        <a href="./market.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Aller sur le marché
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col s6 m6 l6">
                        <a href="./loan.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Faire un prêt
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html>