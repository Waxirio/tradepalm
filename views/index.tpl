<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar.tpl'}
        </header>

        <main class="center">
            <!-- Image and title on it -->
            <div class="image-container">
                <img style="width:100%;" src="../img/bandeau.png" alt="bandeau">
                <h4 class="title-on-image center main-color-text hide-on-large-only">Gestionnaire de Palmiers</h4>
                <h2 class="title-on-image center main-color-text hide-on-med-and-down">Gestionnaire de Palmiers</h2>
                <div class="tips-on-image main-color-text hide-on-med-and-down">
                    Bienvenu sur le compteur de palmiers ! <br>
                    Je vous invite à créer votre compte et ainsi accéder à la gestion de vos échanges en palmiers !
                </div>
            </div>

            <!-- pro tips for beginning -->
            <div class="card main-color">
                <div class="card-content white-text center hide-on-large-only">
                    Bienvenu sur le compteur de palmiers ! <br>
                    Je vous invite à créer votre compte et ainsi accéder à la gestion de vos échanges en palmiers !
                </div>
            </div>

            <!-- Possibility to subscribe or connect -->
            <div class="row">
                <div class="col s6 m6 l6">
                    <a href="../php/about.php">
                        <div class="card main-color hoverable">
                            <div class="card-content white-text center">
                                <div>Qu'es-ce que c'est ?</div>
                            </div>
                        </div>
                    </a>
                    <a href="../php/signup.php">
                        <div class="card main-color hoverable">
                            <div class="card-content white-text center">
                                <div>Inscription</div>
                            </div>
                        </div>
                    </a>
                    <a href="../php/signin.php">
                        <div class="card main-color hoverable">
                            <div class="card-content white-text center">
                                <div>Connexion</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s6 m6 l6">
                    <div class="card main-color">
                        <div class="card-content center">
                            <table>
                                <thead class="white-text">
                                    <tr>
                                        <th>Quelques chiffres</th>
                                    </tr>
                                </thead>

                                <tbody class="white-text">
                                    <tr>
                                        <td>Inscrits</td>
                                        <td>{$nbInscrit}</td>
                                    </tr>
                                    <tr>
                                        <td>Echanges effectués</td>
                                        <td>{$nbTrade}</td>
                                    </tr>
                                    <tr>
                                        <td>Palmiers échangés</td>
                                        <td>{$nbPalmTraded}</td>
                                    </tr>
                                    <tr>
                                        <td>Papillotes échangés</td>
                                        <td>{$nbPalpTraded}</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html>