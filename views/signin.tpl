<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar.tpl'}
        </header>

        <main>
            <div class="container">
                {if isset($msg)}
                    <div class="card red-color-border  center">                       
                        <span>{$msg}</span>               
                    </div>
                {/if}
            </div>
            <div class="row hide-on-med-and-down">
                <div class="main-color-text center col s4 l4 m4">
                    <h2>Login</h2>
                    <img class="palm-image center" src="../img/palm.png" alt="marvelous palm" />
                    <div>Gérez vos palmiers en détails où que vous soyez</div>
                </div>
                <div class="col s8 m8 l8 left-align">
                    <div class="card main-color-border log-form">
                        <form action="../php/p_signin.php" method="post">
                            <h3 class="center" >Connexion</h3>
                            <div class="divider"></div>
                            <div class="container main-color-text">
                                <div class="row">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">person</i>
                                        <input name="login" id="icon_prefix" type="text" class="validate">
                                        <label for="icon_prefix">Pseudo</label>
                                    </div>

                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">lock</i>
                                        <input name="pass" id="icon_pass" type="password" class="validate">
                                        <label for="icon_pass">Password</label>
                                    </div>
                                
                                    <button class="right btn main-color" type="submit" name="action">
                                        Connexion
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <div class="hide-on-large-only">
                    <div class="container">
                        <div class="card main-color-border center">
                            <div class="row center">
                                <form class="col s12 m12 l12" action="../php/p_signin.php" method="post">
                                    <div class="container main-color-text">
                                        <h4>Connectez vous avec votre pseudo</h4>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">person</i>
                                                <input name="login" id="icon_prefix" type="text" class="validate">
                                                <label for="icon_prefix">Pseudo</label>
                                            </div>

                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">lock</i>
                                                <input name="pass" id="icon_pass" type="password" class="validate">
                                                <label for="icon_pass">Password</label>
                                            </div>
                                        </div>
                                    
                                        <button class="right btn main-color" type="submit" name="action">
                                            Connexion
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </main>
    </body>

    <footer>
    </footer>
</html>