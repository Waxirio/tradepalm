<html>

    <head>
        <title>{$title}</title>
        {include file='../layout/_base.tpl'}
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            {include file='../layout/_nav_bar.tpl'}
        </header>

        <main>
            
        </main>
    </body>

    <footer>
    </footer>
</html>