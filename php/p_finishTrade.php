<?php

    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Trade.php');

    $title = "Process";

    //give title
    $smarty->assign('title', $title);
    //if the form was submitted
    //if the submission contain right informations
    if(isset($_POST["id"])){
        //if id is right (numeric and integer)
        if(is_numeric($_POST["id"]) && is_int(intval($_POST["id"]))){
            $id = $_POST["id"];
            if(isset($_POST['cancel'])){
                //if user cancel the trade
                Trade::cancelTransaction($id);
            }
            elseif($_POST['accept']){
                //if user validate the trade
                Trade::validTransaction($id);
            }
        }
        //set error
        $_SESSION['err'] = "Les données du receveur ou de l'envoyeur sont invalide";
        //go to menu
        header('Location: trade.php');
    }
    else{
        //redirect if not good
        //set error
        $_SESSION['err'] = "Les données du receveur est manquante";
        //go to menu
        header('Location: trade.php');
    }
?>