<?php
    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Debt.php');

    $title = "Process";

    //give title
    $smarty->assign('title', $title);

    //if user is connected and we have all informations
    if(isset($_SESSION['id']) && isset($_POST["amountLoanPalm"]) && isset($_POST["amountLoanPap"]) && isset($_POST["id"])){
        //if it's a number
        if(is_numeric($_POST["amountLoanPalm"]) && is_numeric($_POST["amountLoanPap"]) && is_numeric($_POST["id"])){
            //convert it
            $amountPalm = intval($_POST["amountLoanPalm"]);
            $amountPap = intval($_POST["amountLoanPap"]);
            $loanId = $_POST["id"];
            //create new Loan
            $payed = Debt::refundDebt($loanId, $amountPalm, $amountPap);
            //if it worked
            if($payed){
                //reload the page
                header('Location: loan.php');
            }
            else{
                //set error
                $_SESSION['err'] = "les valeurs ne sont pas valide";
                //go to menu
                header('Location: welcome.php');
            }
            
        }
        else{
            //set error
            $_SESSION['err'] = "La valeur donnée n'est pas un nombre";
            //go to menu
            header('Location: welcome.php');
        }
    }
    else{
        //set error
        $_SESSION['err'] = "Le remboursement n'a pas pu être effectué (Données manquante)";
        //go to menu
        header('Location: welcome.php');
    }
?>