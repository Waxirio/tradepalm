<?php

    require_once('header.php');
    require_once('class/User.php');

    $title = "Process";

    //give title
    $smarty->assign('title', $title);
    //if the form was submitted
    //if the submission contain login, pass and email
    $msg = "L'utilisateur est crée";
    if(isset($_POST["login"]) && isset($_POST["pass"]) && isset($_POST["cpass"]) && isset($_POST["email"]) ){
        //get all information (almost safety)
        $login = $_POST["login"];
        $pass = $_POST["pass"];
        $cpass = $_POST["cpass"];
        $email = $_POST["email"];
        //If a user has already the same name
        if(User::exists($login)){
            // give an error message
            $msg = "Le nom d'utilisateur est déjà prit";
            //give error message
            $smarty->assign('msg', $msg);
            //Display smarty page
            $smarty->display('signup.tpl');
        }
        else{
            //if password aren't the same
            if($pass != $cpass){
                $msg = "Les deux mots de passe ne se correspondent pas";
                //give error message
                $smarty->assign('msg', $msg);
                //Display smarty page
                $smarty->display('signup.tpl');
            }
            else{
                // we can create a new user
                try{
                    //if referral code
                    $referral = NULL;
                    //add referral to user
                    if(isset($_POST["referral"])){
                        //protect string
                        $referral = htmlspecialchars($_POST["referral"]);
                    }
                    User::createUser($login, $pass, $email, $referral);
                    $user = User::getUserByLogin($login);
                    //give user
                    $smarty->assign('user', $user);
                    //give error message
                    $smarty->assign('msg', $msg);
                    //Display smarty page
                    $smarty->display('signin.tpl');
                }
                catch(Exception $e){
                    echo $e;
                }
            }
        }
    }
    else{
        //say that there is a problem because of undefined variable sent
        $msg = "Erreur lors de l'envoie du formulaire";
        //give error message
        $smarty->assign('msg', $msg);
        //Display smarty page
        $smarty->display('signup.tpl');
    }
?>