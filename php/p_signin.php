<?php

    require_once('header.php');
    require_once('class/User.php');

    $title = "Process";

    //give title
    $smarty->assign('title', $title);
    //if the form was submitted
    //if the submission contain login, pass and email
    $msg = "Connection effectuée";
    if(isset($_POST["login"]) && isset($_POST["pass"])){
        //get all information (almost safety)
        $login = $_POST["login"];
        $pass = $_POST["pass"];
        //If a user has already the same name
        if(!User::exists($login)){
            // give an error message
            $msg = "Le nom d'utilisateur est inexistant";
            //give error message
            $smarty->assign('msg', $msg);
            //Display smarty page
            $smarty->display('signin.tpl');
        }
        else{
            //if password aren't the same
            if(User::isRegistered($login, $pass)){
                $user = User::getUserByLogin($login);
                //create session
                $_SESSION['id'] = $user->getId();
                //Display smarty page
                header('Location: welcome.php'); 
            }
            else{
                // we custom the error message
                $msg = "Le mot de passe est incorrect";
                //give error message
                $smarty->assign('msg', $msg);
                //Display smarty page
                $smarty->display('signin.tpl');
            }
        }
    }
    else{
        //say that there is a problem because of undefined variable sent
        $msg = "Erreur lors de l'envoie du formulaire";
        //give error message
        $smarty->assign('msg', $msg);
        //Display smarty page
        $smarty->display('signin.tpl');
    }
?>