<?php

    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Trade.php');

    $title = "Process";

    //give title
    $smarty->assign('title', $title);
    //if the form was submitted
    //if the submission contain login, pass and email
    if(isset($_SESSION['id']) && isset($_POST["id"]) && isset($_POST["quantitePalm"]) && isset($_POST["quantitePap"]))
    {
        //get all information (almost safety)
        $idReceiver = $_POST["id"];
        $quantitePalm = $_POST["quantitePalm"];
        $quantitePap = $_POST["quantitePap"];
        $idSender = $_SESSION['id'];
        //If a user has already the same name
        if(User::existsId($idReceiver) && is_numeric($quantitePalm) && is_numeric($quantitePap) && $idReceiver != $idSender){
            //transcrire le string en entier
            $quantitePalm = intval($quantitePalm);
            $quantitePap = intval($quantitePap);
            if(is_int($quantitePalm)){
                //create new transaction
                $worked = Trade::createTransaction($idSender, $idReceiver, $quantitePalm, $quantitePap);
                //if it worked
                if($worked){
                    //Display smarty page
                    header('Location: trade.php');
                }
                else{
                    //set error
                    $_SESSION['err'] = "La transaction n'a pas pu être effectuée (Données invalide)";
                    //go to menu
                    header('Location: welcome.php');
                }
            }
            else{
                //set error
                $_SESSION['err'] = "La quantité spécifiée est invalide (entier attendu)";
                //go to menu
                header('Location: welcome.php');
            }
        }
        else{
            //set error
            $_SESSION['err'] = "Les données du receveur ou la quantite sont invalide";
            //go to menu
            header('Location: welcome.php');
        }
    }
    else{
        //set error
        $_SESSION['err'] = "Une ou plusieurs données transmises sont manquantes";
        //go to menu
        header('Location: welcome.php');
    }
?>