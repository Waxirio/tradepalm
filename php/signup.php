<?php

    require_once('header.php');

    $title = "Sign up";

    //give title
    $smarty->assign('title', $title);

    //Display smarty page
    if(isset($_SESSION['id'])){
        header('Location: ./welcome.php'); 
    }
    else{
        $smarty->display('signup.tpl');
    }
?>