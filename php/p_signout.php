<?php

    require_once('header.php');

    //finish the session variable
    session_destroy();

    //redirect to connexion page
    header('Location: signin.php');

?>