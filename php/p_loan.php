<?php
    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Debt.php');

    $title = "Process";

    //give title
    $smarty->assign('title', $title);

    //if user is connected and we have all informations
    if(isset($_SESSION['id']) && isset($_POST["amountPalm"]) && isset($_POST["amountPap"])){
        //if it's a number
        if(is_numeric($_POST["amountPalm"]) && is_numeric($_POST["amountPap"])){
            //convert it
            $amountPalm = intval($_POST["amountPalm"]);
            $amountPap = intval($_POST["amountPap"]);
            //create new Loan
            Debt::createDebt($_SESSION['id'], $amountPalm, $amountPap);
            //reload the page
            header('Location: loan.php');
        }
        else{
            //set error
            $_SESSION['err'] = "La valeur donnée n'est pas valide";
            //go to menu
            header('Location: welcome.php');
        }
    }
    else{
        //set error
        $_SESSION['err'] = "La transaction n'a pas pu être effectuée (Données manquante)";
        //go to menu
        header('Location: welcome.php');
    }
?>