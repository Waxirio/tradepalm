<?php

    require_once('header.php');
    require_once('class/User.php');

    $title = "Send Palm";

    //give title
    $smarty->assign('title', $title);

    //if the connection is true
    if(isset($_SESSION['id'])){
        //get user with the id
        $user = User::getUserById($_SESSION['id']);
        //give user to page
        $smarty->assign('user', $user);
        if(isset($_POST['search'])){
            //secure the word given by user
            $search = htmlspecialchars($_POST['search']);
            //search user with login
            $userLogin = User::getSomeUserWithLogin($search);
            //feed the page with login searched user
            $smarty->assign('userLogin', $userLogin);
            //search user with walletId
            $userWallet = User::getSomeUserWithWalletId($search);
            //feed the page with wallet Id
            $smarty->assign('userWallet', $userWallet);
            //feed with the search string sample
            $smarty->assign('search', $search);
        }
        //display page
        $smarty->display('userlist.tpl');
    }
    else{
        //else redirect to signin page
        header('Location: signin.php'); 
    }
    
?>