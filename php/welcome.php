<?php

    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Trade.php');

    $title = "Dashboard";

    //give title
    $smarty->assign('title', $title);

    //if the connection is true
    if(isset($_SESSION['id'])){
        //if there is an error message
        if(isset($_SESSION['err'])){
            //then escape string
            $err = htmlspecialchars($_SESSION['err']);
            //transmit info to page
            $smarty->assign('err', $err);
        }
        //get user with the id
        $user = User::getUserById($_SESSION['id']);
        //give user to page
        $smarty->assign('user', $user);
        //Palmiers en jeu
        $smarty->assign('restPalm', Trade::getPalmTransaction($_SESSION['id']));
        //display page
        $smarty->display('welcome.tpl');
    }
    else{
        //else redirect to signin page
        header('Location: signin.php'); 
    }
    unset($_SESSION['err']);
?>