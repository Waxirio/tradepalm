<?php

require_once 'PalmDb.php';
require_once 'User.php';
require_once 'Trade.php';

class Market {
    public $_id;
    public $_palm;
    public $_pap;
    public $_createdAt;
    const TABLE = "market";
   
    // ********** Constructeur **********

    function __construct() {
    }

    // ********** Utils **********

    //get last value for market
    public static function getLastValue(){
        //create new user
        $market = new self();
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $market_info = array();
        //do query to get all info of market
        if ($stmt = $mysqli->prepare("SELECT * FROM ".self::TABLE." ORDER BY ID DESC LIMIT 1")) {
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_assoc()) {
                //store it into a table
                $market_info = array (
                    "id"  => $row['id'],
                    "palm" => $row['nb_palm'],
                    "pap" => $row['nb_pap'],
                    "createdAt" => $row['created_at'],
                );
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        //and create new debt
        $market->fill($market_info);
        //return new debt
        return $market;
    }

    //rempli une instance de DEBT avec les bone elements contenus dans un tableau
    protected function fill(array $info){
        $this->_id = $info['id'];
        $this->_palm = $info['palm'];
        $this->_pap = $info['pap'];
        $this->_createdAt = $info['createdAt'];
    }

    // ********** Creation des getters **********

    public function getId() {
        return $this->_id;
    }

    public function getPalm() {
        return $this->_palm;
    }

    public function getPap() {
        return $this->_pap;
    }

    public function getDate(){
        return $this->_createdAt;
    }
} 

?>