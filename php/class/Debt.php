<?php

require_once 'PalmDb.php';
require_once 'User.php';
require_once 'Trade.php';

class Debt {
    public $_id;
    public $_idUser;
    public $_debtPalm;
    public $_debtPap;
    public $_payPalm;
    public $_payPap;
    public $_createdAt;
    public $_finishedAt;
    const TABLE = "debt";
   
    // ********** Constructeur **********

    function __construct() {
    }

    // ********** Utils **********

    public static function getDebtId($id){
        //create new user
        $debt = new self();
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $debt_info = array();
        //do query to get all info of debt
        if ($stmt = $mysqli->prepare("SELECT * FROM ".self::TABLE." WHERE id=?")) {
            // bind parameters for markers 
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_assoc()) {
                //store it into a table
                $debt_info = array (
                    "id"  => $row['id'],
                    "idU" => $row['id_user'],
                    "dPalm" => $row['debt_palm'],
                    "dPap" => $row['debt_pap'],
                    "pPalm" => $row['pay_palm'],
                    "pPap" => $row['pay_pap'],
                    "createdAt" => $row['created_at'],
                    "finishedAt" => $row['finished_at'],
                );
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        //and create new debt
        $debt->fill($debt_info);
        //return new debt
        return $debt;
    }

    public static function createDebt($id, $amountPalm, $amountPap){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //final variable
        $created = false;
        $zero = 0;
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("INSERT INTO ".self::TABLE." (id_user, debt_palm, debt_pap, pay_palm, pay_pap,created_at) 
                                      VALUES (?, ?, ?, ?, ?, NOW())"))
        {
            // bind parameters for values
            $stmt->bind_param("iiiii", $id, $amountPalm, $amountPap, $zero, $zero);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $created = true;
            }
            else{
                $created = false;
            }
            // close statement
            $stmt->close();
        }
        if($created){
            $created = User::addPalm($id, $amountPalm);
            $created = User::addPap($id, $amountPap);
        }
        // close connection
        $mysqli->close();
        //return the right value
        return $created;
    }

    //rembourse une dette
    public static function refundDebt($loanId, $amountPalm, $amountPap){
        //instanciate result
        $res = false;
        //get Debt
        $debt = Debt::getDebtId($loanId);
        //get user
        $user = User::getUserById($debt->getIdU());
        //if the amount is valid
        if(Trade::isValide($user->getId(), $amountPalm, $amountPap) && 
           ($amountPalm <= ($debt->getDebtPalm() - $debt->getPayPalm())) && 
           ($amountPap <= ($debt->getDebtPap() - $debt->getPayPap())) &&
           ($amountPalm > 0 || $amountPap > 0))
        {
            //Add palm into the debt
            $ok = $debt->addPalmPay($amountPalm, $amountPap);
            if($ok){
                //check if the loan is finished
                $realPaymentPalm = $amountPalm;
                $realPaymentPap = $amountPap;
                //do the deal
                User::addPalm($user->getId(), ($realPaymentPalm * -1));
                User::addPap($user->getId(), ($realPaymentPap * -1));
                //if the debt is totally payed
                //instanciate res
                $res = true;
                if(($debt->getDebtPalm() == $debt->getPayPalm()) && ($debt->getDebtPap() == $debt->getPayPap())){
                    //finish the debt
                    if(!$debt->valideDebt()){
                        //instanciate res
                        $res = false;
                    }
                }
            }
        }
        //return result
        return $res;
    }

    //change palm number of a debt
    protected function addPalmPay($amountPalm, $amountPap){
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //update amount of palm
        $newAmountPalm = $this->getPayPalm() + $amountPalm;
        $newAmountPap = $this->getPayPap() + $amountPap;
        $this->_payPalm = $newAmountPalm;
        $this->_payPap = $newAmountPap;
        //get id of the debt
        $id = $this->getId();
        $res = false;
        //begin connection
        if ($stmt = $mysqli->prepare("UPDATE ".self::TABLE." SET pay_palm = ?, pay_pap = ? WHERE id = ?")) {
            // bind parameters for markers 
            $stmt->bind_param("iii", $newAmountPalm, $newAmountPap, $id);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $res = true;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $res;
    }

    //rembourse une dette
    protected function valideDebt(){
        //get connexion string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //set res variable
        $res = false;
        //set id for query
        $id = $this->getId();
        //begin transaction
        if ($stmt = $mysqli->prepare("UPDATE ".self::TABLE." SET finished_at = NOW() WHERE id = ?"))
        {
            // bind parameters for values
            $stmt->bind_param("i", $id);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $res = true;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        return $res;
    }

    //récupère les trades d'un user
    public static function getDebtByUserId($id){
        //create new connection
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $debt_info = array();
        $debts = array();
        //do query to get all info of trade
        if ($stmt = $mysqli->prepare("SELECT * 
                                    FROM ".self::TABLE." 
                                    WHERE id_user = ?"))
        {
            // bind parameters for markers 
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_array()) {
                $debt = new self();
                //store it into a table
                $debt_info = array (
                    "id"  => $row['id'],
                    "idU" => $row['id_user'],
                    "dPalm" => $row['debt_palm'],
                    "dPap" => $row['debt_pap'],
                    "pPalm" => $row['pay_palm'],
                    "pPap" => $row['pay_pap'],
                    "createdAt" => $row['created_at'],
                    "finishedAt" => $row['finished_at'],
                );
                //and create new trade
                $debt->fill($debt_info);
                //and fill users tab
                array_push($debts, $debt);
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        //return new user
        return $debts;
    }

    //rempli une instance de DEBT avec les bone elements contenus dans un tableau
    protected function fill(array $info){
        $this->_id = $info['id'];
        $this->_idUser = $info['idU'];
        $this->_debtPalm = $info['dPalm'];
        $this->_debtPap = $info['dPap'];
        $this->_payPalm = $info['pPalm'];
        $this->_payPap = $info['pPap'];
        $this->_createdAt = $info['createdAt'];
        $this->_finishedAt = $info['finishedAt'];
    }

    // ********** Creation des getters **********

    public function getId() {
        return $this->_id;
    }

    public function getIdU() {
        return $this->_idUser;
    }

    public function getDebtPalm() {
        return $this->_debtPalm;
    }

    public function getDebtPap() {
        return $this->_debtPap;
    }

    public function getPayPalm() {
        return $this->_payPalm;
    }

    public function getPayPap() {
        return $this->_payPap;
    }

    public function getCreatedAt() {
        return $this->_createdAt;
    }

    public function getFinishedAt() {
        return $this->_finishedAt;
    }
} 

?>