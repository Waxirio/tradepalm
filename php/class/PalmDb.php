<?php

class PalmDb {
    public $_ip;
    public $_user;
    public $_pass;
    public $_table;
    public static $instance = NULL;

    public static function getInstance(){
        //return the only instance of DB manager
        if(PalmDb::$instance == NULL){
            PalmDb::$instance = new PalmDb();
            PalmDb::$instance->_ip = "localhost";
            PalmDb::$instance->_user = "palm";
            PalmDb::$instance->_pass = "palmcounter";
            PalmDb::$instance->_table = "palm";
        }
        return PalmDb::$instance;
    }

    public function getSQLI(){
        //Get connection string for a given table
        $mysqli = new mysqli($this->_ip, $this->_user, $this->_pass, $this->_table);
        //If there is an error
        if (mysqli_connect_errno()) {
            //Exit and throw an errror
            printf("Connect failed: %s\n", mysqli_connect_error());
            exit();
        }
        //else, give the working connection string
        return $mysqli;
    }
}

?>