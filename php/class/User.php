<?php

require_once 'PalmDb.php';

class User {
    public $_id;
    public $_walletId;
    public $_referral;
    public $_login;
    public $_email;
    public $_pass;
    public $_palm;
    public $_pap;
    public $_createdAt;
    public $_lastConnection;
    const TABLE = "users";
   
    // ********** Constructeur **********

    function __construct() {
    }

    // ********** Utils **********

    //Renvoie si l'utilisateur existant a déjà le meme pseudo
    public static function exists($login){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $login = strtoupper($mysqli->real_escape_string($login));
        //do query to get all info of user
        $res = NULL;
        if ($stmt = $mysqli->prepare("SELECT id FROM ".self::TABLE." WHERE UPPER(pseudo)=?")) {
            // bind parameters for markers 
            $stmt->bind_param("s", $login);
            // execute query 
            $stmt->execute();
            // bind result variables
            $stmt->bind_result($id);
            // fetch value
            while ($stmt->fetch()) {
                $res = $id;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $res != NULL;
    }

    //Renvoie si l'utilisateur existant a déjà le meme pseudo
    public static function existsId($id){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //do query to get all info of user
        $res = NULL;
        if ($stmt = $mysqli->prepare("SELECT id FROM ".self::TABLE." WHERE id=?")) {
            // bind parameters for markers 
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $stmt->bind_result($id);
            // fetch value
            while ($stmt->fetch()) {
                $res = $id;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $res != NULL;
    }

    public static function getNbInscrits(){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //set result variable
        $res = 0;
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT id FROM ".self::TABLE)){
            //execution of the query
            $stmt->execute();
            //get result
            $result = $stmt->get_result();
            //get nb subscribers
            $res = $result->num_rows;
        }
        //return nb users
        return $res;
    }

    //Retourne vrai si l'utilisateur est dans la base et que le mot de passe et login correspondent
    public static function isRegistered($login, $pass){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $login = strtoupper($mysqli->real_escape_string($login));
        $pass = $mysqli->real_escape_string($pass);
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT * FROM users WHERE UPPER(pseudo) = ?"))
        {
            $stmt->bind_param("s", $login);
            $stmt->execute();
            //get all informations
            $user = $stmt->get_result()->fetch_assoc();
            //verify password to connect
            if ($user && password_verify($pass, $user['pass']))
            {
                return true;
            } else {
                return false;
            }
        }
    }
    
    //recuperer le bon id du referral
    public static function getRefferal($code){
        //instanciate code
        $res = NULL;
        //get sql string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //prepare query
        if ($stmt = $mysqli->prepare("SELECT id FROM ".self::TABLE." WHERE wallet_id = ?")){
            //bind param
            $stmt->bind_param("s", $code);
            //execution of the query
            $stmt->execute();
            //get result
            $stmt->bind_result($id);
            //fetch data
            while ($stmt->fetch()) {
                //get result
                $res = $id;
            }
        }

        return $res;
    }

    //créer un utilisateur avec les trois éléments de l'inscription
    public static function createUser($login, $pass, $email, $referral){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //hash password for security
        $hashedPassword = password_hash($pass, PASSWORD_DEFAULT);
        //final variable
        $referral_id = User::getRefferal($mysqli->real_escape_string($referral));
        $created = false;
        $nbPalm = 0;
        $nbPap = 50;
        if($referral_id != NULL){
            $nbPap += 25;
            User::addPap($referral_id, 25);
        }
        $lastConnection = NULL;
        $walletId = sha1($login);
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("INSERT INTO ".self::TABLE." (wallet_id, referral_id, pseudo, email, pass, palmiers, papillotes, created_at, last_connection) 
                                      VALUES (?, ?, ?, ?, ?, ?, ?, NOW(), ?)"))
        {
            // bind parameters for values
            $stmt->bind_param("sisssiis", $walletId, $referral_id, $login, $email, $hashedPassword, $nbPalm, $nbPap, $lastConnection);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $created = true;
            }
            else{
                $created = false;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $created;
    }

    //récupere un user avec le login donné
    public static function getUserByLogin($login){
        //create new user
        $user = new self();

        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $login = strtoupper($mysqli->real_escape_string($login));
        $user_info = array();
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT * FROM ".self::TABLE." WHERE UPPER(pseudo)=?")) {
            // bind parameters for markers 
            $stmt->bind_param("s", $login);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_assoc()) {
                //store it into a table
                $user_info = array (
                    "id"  => $row['id'],
                    "walletId" => $row['wallet_id'],
                    "login"   => $row['pseudo'],
                    "email" => $row['email'],
                    "palm" => $row['palmiers'],
                    "pap" => $row['papillotes'],
                    "createdAt" => $row['created_at'],
                    "lastConnection" => $row['last_connection']
                );
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        //and create new user
        $user->fill($user_info);
        //return new user
        return $user;
    }

    //change palm number of a user
    public static function addPalm($id, $amount){
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //get user we want
        $user = User::getUserById($id);
        //update amount of palm
        $newAmount = $user->getPalm() + $amount;
        $res = false;
        //begin connection
        if ($stmt = $mysqli->prepare("UPDATE ".self::TABLE." SET palmiers = ? WHERE id = ?")) {
            // bind parameters for markers 
            $stmt->bind_param("ii", $newAmount, $id);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $res = true;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $res;
    }

    //change pap number of a user
    public static function addPap($id, $amount){
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //get user we want
        $user = User::getUserById($id);
        //update amount of palm
        $newAmount = $user->getPap() + $amount;
        $res = false;
        //begin connection
        if ($stmt = $mysqli->prepare("UPDATE ".self::TABLE." SET papillotes = ? WHERE id = ?")) {
            // bind parameters for markers 
            $stmt->bind_param("ii", $newAmount, $id);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $res = true;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $res;
    }

    //get name of a user with id
    public static function getName($id){
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //instanciate res string;
        $res = "";
        //begin connection
        if ($stmt = $mysqli->prepare("SELECT pseudo FROM ".self::TABLE." WHERE id=?")) {
            // bind parameters for markers 
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind results
            $stmt->bind_result($login);
            // fetch value
            while ($stmt->fetch()) {
                $res = $login;
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        return $res;
    }

    //récupère un user ayant l'id donné
    public static function getUserById($id){
        //create new user
        $user = new self();
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $user_info = array();
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT * FROM ".self::TABLE." WHERE id=?")) {
            // bind parameters for markers 
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_assoc()) {
                //store it into a table
                $user_info = array (
                    "id"  => $row['id'],
                    "walletId" => $row['wallet_id'],
                    "login"   => $row['pseudo'],
                    "email" => $row['email'],
                    "palm" => $row['palmiers'],
                    "pap" => $row['papillotes'],
                    "createdAt" => $row['created_at'],
                    "lastConnection" => $row['last_connection']
                );
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        //and create new user
        $user->fill($user_info);
        //return new user
        return $user;
    }

    //récupère un user ayant l'id donné
    public static function getSomeUserWithLogin($login){
        //create new user
        $mysqli = PalmDb::getInstance()->getSQLI();
        $login = strtoupper($mysqli->real_escape_string($login));
        //Protect database of injection
        $user_info = array();
        $users = array();
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT * 
                                    FROM ".self::TABLE." 
                                    WHERE UPPER(pseudo) LIKE CONCAT('%',?,'%')
                                    ORDER BY pseudo asc
                                    limit 20"))
        {
            // bind parameters for markers 
            $stmt->bind_param("s", $login);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_array()) {
                $user = new self();
                //store it into a table
                $user_info = array (
                    "id"  => $row['id'],
                    "walletId" => $row['wallet_id'],
                    "login"   => $row['pseudo'],
                    "email" => $row['email'],
                    "palm" => $row['palmiers'],
                    "pap" => $row['papillotes'],
                    "createdAt" => $row['created_at'],
                    "lastConnection" => $row['last_connection']
                );
                //and create new user
                $user->fill($user_info);
                //and fill users tab
                array_push($users, $user);
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        //return new user
        return $users;
    }

    //récupère un user ayant l'id donné
    public static function getSomeUserWithWalletId($walletId){
        //create new user
        $mysqli = PalmDb::getInstance()->getSQLI();
        $login = strtoupper($mysqli->real_escape_string($walletId));
        //Protect database of injection
        $user_info = array();
        $users = array();
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT * 
                                    FROM ".self::TABLE." 
                                    WHERE UPPER(wallet_id) LIKE CONCAT('%',?,'%')
                                    ORDER BY pseudo asc
                                    limit 20"))
        {
            // bind parameters for markers 
            $stmt->bind_param("s", $walletId);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_array()) {
                $user = new self();
                //store it into a table
                $user_info = array (
                    "id"  => $row['id'],
                    "walletId" => $row['wallet_id'],
                    "login"   => $row['pseudo'],
                    "email" => $row['email'],
                    "palm" => $row['palmiers'],
                    "pap" => $row['papillotes'],
                    "createdAt" => $row['created_at'],
                    "lastConnection" => $row['last_connection']
                );
                //and create new user
                $user->fill($user_info);
                //and fill users tab
                array_push($users, $user);
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        //return new user
        return $users;
    }

    //rempli une instance de USER avec les bone elements contenus dans un tableau
    protected function fill(array $info){
        $this->_id = $info['id'];
        $this->_walletId = $info['walletId'];
        $this->_login = $info['login'];
        $this->_email = $info['email'];
        //password but we don't want it 
        $this->_pap = $info['pap'];       
        $this->_palm = $info['palm'];
        $this->_createdAt = $info['createdAt'];
        $this->_lastConnection = $info['lastConnection'];
    }

    // ********** Creation des getters **********

    public function getId() {
        return $this->_id;
    }

    public function getWalletId() {
        return $this->_walletId;
    }

    public function getLogin() {
        return $this->_login;
    }

    public function getPass() {
        return $this->_pass;
    }

    public function getEmail() {
        return $this->_email;
    }

    public function getPalm() {
        return $this->_palm;
    }

    public function getPap() {
        return $this->_pap;
    }

    public function getCreatedAt() {
        return $this->_createdAt;
    }
} 

?>