<?php

require_once 'PalmDb.php';
require_once 'User.php';
require_once 'Trade.php';

class Market {
    public $_id;
    public $_idUser;
    public $_palm;
    public $_pap;
    public $_type;
    public $_didAt;
    const TABLE = "market_trade";
   
    // ********** Constructeur **********

    function __construct() {
    }

    // ********** Utils **********

    //get last value for market
    public static function getAllMarketTrade($id){
        //create new user
        $market = new self();
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $market_info = array();
        //do query to get all info of market
        if ($stmt = $mysqli->prepare("SELECT * FROM ".self::TABLE." WHERE id_user = ?")) {
            // bind parameters for values
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            $trades = array();
            // fetch value
            while ($row = $result->fetch_assoc()) {
                $trade = new self();
                //store it into a table
                $market_info = array (
                    "id"  => $row['id'],
                    "idU" => $row['id_user'],
                    "palm" => $row['nb_palm'],
                    "pap" => $row['nb_pap'],
                    "type" => $row['type'],
                    "didAt" => $row['did_at'],
                );

                $trade->fill($market_info);
                //and fill users tab
                array_push($trades, $trade);
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();

        //and create new debt
        $market->fill($market_info);
        //return new debt
        return $trades;
    }

    //rempli une instance de DEBT avec les bone elements contenus dans un tableau
    protected function fill(array $info){
        $this->_id = $info['id'];
        $this->_idUser = $info['idU'];
        $this->_palm = $info['palm'];
        $this->_pap = $info['pap'];
        $this->_type = $info['type'];
        $this->_didAt = $info['didAt'];
    }

    // ********** Creation des getters **********

    public function getId() {
        return $this->_id;
    }

    public function getIdU() {
        return $this->_idUser;
    }

    public function getPalm() {
        return $this->_palm;
    }

    public function getPap() {
        return $this->_pap;
    }

    public function getType() {
        return $this->_type;
    }

    public function getDate() {
        return $this->_didAt;
    }
} 

?>