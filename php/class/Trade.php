<?php

require_once 'PalmDb.php';
require_once 'User.php';

class Trade {
    public $_id;
    public $_idGiver;
    public $_giverName;
    public $_idReceiver;
    public $_receiverName;
    public $_amountPalm;
    public $_amountPap;
    public $_validate;
    public $_createdAt;
    public $_validateAt;
    const TABLE = "transactions";
   
    // ********** Constructeur **********

    function __construct() {
    }

    // ********** Utils **********

    public static function cancelTransaction($id){
        //get connection string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //result
        $res = false;
        //go to query
        if ($stmt = $mysqli->prepare("DELETE FROM ".self::TABLE." WHERE id = ?"))
        {
            // bind parameters for values
            $stmt->bind_param("i", $id);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $res = true;
            }
            // close statement
            $stmt->close();
        }

        return $res;
    }

    public static function getNbPapTraded(){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //set result variable
        $res = 0;
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT amount_pap FROM ".self::TABLE)){
            //execution of the query
            $stmt->execute();
            //get result
            $result = $stmt->get_result();
            //loop into result
            while ($row = $result->fetch_array()) {
                //add palm
                $res += $row['amount_pap'];
            }
        }

        return $res;
    }

    //get nb transaction
    public static function getNbPalmTraded(){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //set result variable
        $res = 0;
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT amount_palm FROM ".self::TABLE)){
            //execution of the query
            $stmt->execute();
            //get result
            $result = $stmt->get_result();
            //loop into result
            while ($row = $result->fetch_array()) {
                //add palm
                $res += $row['amount_palm'];
            }
        }

        return $res;
    }

    //get nb transaction
    public static function getNbTrades(){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //set result variable
        $res = 0;
        //do query to get all info of user
        if ($stmt = $mysqli->prepare("SELECT id FROM ".self::TABLE)){
            //execution of the query
            $stmt->execute();
            //get result
            $result = $stmt->get_result();
            //get nb subscribers
            $res = $result->num_rows;
        }

        return $res;
    }

    public static function validTransaction($id){
        //get wanted trade
        $trade = Trade::getTradeById($id);
        //get connexion string
        $mysqli = PalmDb::getInstance()->getSQLI();
        //set res variable
        $res = false;
        //begin transaction
        if ($stmt = $mysqli->prepare("UPDATE ".self::TABLE." SET validate = 1, validate_at = NOW() WHERE id = ?"))
        {
            // bind parameters for values
            $stmt->bind_param("i", $id);
            // execute query 
            $ok = $stmt->execute();
            if($ok){
                $res = true;
            }
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        //if everything alright then do operation
        if($res){
            //Giver amount remove palm
            $amountGPalm = $trade->getAmountPalm() * -1;
            //Receiver Amount add palm
            $amountRPalm = $trade->getAmountPalm();
            //Giver amount remove palm
            $amountGPap = $trade->getAmountPalm() * -1;
            //Receiver Amount add palm
            $amountRPap = $trade->getAmountPalm();
            //Change values
            User::addPalm($trade->getIdG(), $amountGPalm);
            User::addPalm($trade->getIdR(), $amountRPalm);
            //Change values
            User::addPap($trade->getIdG(), $amountGPap);
            User::addPap($trade->getIdR(), $amountRPap);
        }
    }

    

    //dit si une transaction peut avoir lieu
    public static function isValide($idGiver, $amountPalm, $amountPap){
        $trades = Trade::getTradeByUserId($idGiver);
        //instanciate result variable
        $res = false;
        //value of palm and pap
        $totalGivePalm = 0;
        $totalGivePap = 0;
        //loop on trades
        foreach ($trades as $t){
            //if it's giver
            if($t->getIdG() == $idGiver){
                $totalGivePalm = $totalGivePalm + $t->getAmountPalm();
                $totalGivePap = $totalGivePap + $t->getAmountPap();
            }
        }
        //get this user
        $user = User::getUserById($idGiver);
        //calcul new amount of given palms
        $newAmountPalm = $amountPalm + $totalGivePalm;
        $newAmountPap = $amountPap + $totalGivePap;
        //say if he can give it more
        if($user->getPalm() >= $newAmountPalm && $user->getPap() >= $newAmountPap && ($amountPalm > 0 || $amountPap > 0)){
            $res = true;
        }
        //return value
        return $res;
    }

    //get total of on user
    public static function getPalmTransaction($id){
        $trades = Trade::getTradeByUserId($id);
        //instanciate result variable
        $res = 0;
        //loop on trades
        foreach ($trades as $t){
            //if it's giver
            if($t->getIdG() == $id && $t->getValidate() == 0){
                $res = $res + $t->getAmount();
            }
        }

        //return value
        return $res;
    }

    //créer un utilisateur avec les trois éléments de l'inscription
    public static function createTransaction($idGiver, $idReceiver, $amountPalm, $amountPap){
        $mysqli = PalmDb::getInstance()->getSQLI();
        //final variable
        $created = false;
        if(Trade::isValide($idGiver, $amountPalm, $amountPap)){
            //do query to get all info of user
            if ($stmt = $mysqli->prepare("INSERT INTO ".self::TABLE." (id_giver, id_receiver, amount_palm, amount_pap, created_at) 
                                        VALUES (?, ?, ?, ?, NOW())"))
            {
                // bind parameters for values
                $stmt->bind_param("iiii", $idGiver, $idReceiver, $amountPalm, $amountPap);
                // execute query 
                $ok = $stmt->execute();
                if($ok){
                    $created = true;
                }
                else{
                    $created = false;
                }
                // close statement
                $stmt->close();
            }
            // close connection
            $mysqli->close();
        }

        return $created;
    }

    //récupère les trades d'un user
    public static function getTradeByUserId($id){
        //create new connection
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $trades_info = array();
        $trades = array();
        //do query to get all info of trade
        if ($stmt = $mysqli->prepare("SELECT * 
                                    FROM ".self::TABLE." 
                                    WHERE id_giver = ? 
                                    OR id_receiver = ?"))
        {
            // bind parameters for markers 
            $stmt->bind_param("ii", $id, $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_array()) {
                $trade = new self();
                //store it into a table
                $trades_info = array (
                    "id"  => $row['id'],
                    "idG" => $row['id_giver'],
                    "idR"   => $row['id_receiver'],
                    "amountPalm" => $row['amount_palm'],
                    "amountPap" => $row['amount_pap'],
                    "validate" => $row['validate'],
                    "createdAt" => $row['created_at'],
                    "validateAt" => $row['validate_at']
                );
                //and create new trade
                $trade->fill($trades_info);
                $trade->fillWithUser();
                //and fill users tab
                array_push($trades, $trade);
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        //return new user
        return $trades;
    }

    //récupère les trades d'un user
    public static function getTradeById($id){
        //create new connection
        $mysqli = PalmDb::getInstance()->getSQLI();
        //Protect database of injection
        $trades_info = array();
        //do query to get all info of trade
        if ($stmt = $mysqli->prepare("SELECT * 
                                    FROM ".self::TABLE." 
                                    WHERE id = ?"))
        {
            // bind parameters for markers 
            $stmt->bind_param("i", $id);
            // execute query 
            $stmt->execute();
            // bind result variables
            $result = $stmt->get_result();
            // fetch value
            while ($row = $result->fetch_array()) {
                $trade = new self();
                //store it into a table
                $trades_info = array (
                    "id"  => $row['id'],
                    "idG" => $row['id_giver'],
                    "idR"   => $row['id_receiver'],
                    "amountPalm" => $row['amount_palm'],
                    "amountPap" => $row['amount_pap'],
                    "validate" => $row['validate'],
                    "createdAt" => $row['created_at'],
                    "validateAt" => $row['validate_at']
                );
                //and create new trade
                $trade->fill($trades_info);
                $trade->fillWithUser();
            }
            //free result memory
            $stmt->free_result();
            // close statement
            $stmt->close();
        }
        // close connection
        $mysqli->close();
        //return new user
        return $trade;
    }

    //récupère les trades d'un user
    protected function fillWithUser(){
        //create new connection
        $this->_giverName = User::getName($this->_idGiver);
        $this->_receiverName = User::getName($this->_idReceiver);
    }

    //rempli une instance de USER avec les bone elements contenus dans un tableau
    protected function fill(array $info){
        $this->_id = $info['id'];
        $this->_idGiver = $info['idG'];
        $this->_idReceiver = $info['idR'];
        $this->_amountPalm = $info['amountPalm'];
        $this->_amountPap = $info['amountPap'];
        $this->_validate = $info['validate'];
        $this->_createdAt = $info['createdAt'];
        $this->_validateAt = $info['validateAt'];
    }

    // ********** Creation des getters **********

    public function getId() {
        return $this->_id;
    }

    public function getIdG() {
        return $this->_idGiver;
    }

    public function getNameG() {
        return $this->_giverName;
    }

    public function getIdR() {
        return $this->_idReceiver;
    }

    public function getNameR() {
        return $this->_receiverName;
    }

    public function getAmountPap() {
        return $this->_amountPap;
    }

    public function getAmountPalm() {
        return $this->_amountPalm;
    }

    public function getValidate() {
        return $this->_validate;
    }

    public function getCreatedAt() {
        return $this->_createdAt;
    }

    public function getValidateAt() {
        return $this->_validateAt;
    }
} 

?>