<?php

    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Trade.php');
    require_once('class/Market.php');

    $title = "Market";

    //give title
    $smarty->assign('title', $title);

    //if the connection is true
    if(isset($_SESSION['id'])){
        //get user with the id
        $user = User::getUserById($_SESSION['id']);
        //give user to page
        $smarty->assign('user', $user);
        //Palmiers en jeu
        $smarty->assign('restPalm', Trade::getPalmTransaction($_SESSION['id']));
        //display page
        $lastValue = Market::getLastValue();
        $smarty->assign('lastValue', $lastValue);
        //get value
        $smarty->display('market.tpl');
    }
    else{
        //else redirect to signin page
        header('Location: signin.php'); 
    }
    unset($_SESSION['err']);
?>