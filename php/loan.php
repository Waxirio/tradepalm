<?php

    require_once('header.php');
    require_once('class/User.php');
    require_once('class/Trade.php');
    require_once('class/Debt.php');

    $title = "Loan";

    //give title
    $smarty->assign('title', $title);
    
    //if the connection is true
    if(isset($_SESSION['id'])){
        //get user with the id
        $user = User::getUserById($_SESSION['id']);
        //give user to page
        $smarty->assign('user', $user);
        //search all transaction of the user
        $debts = Debt::getDebtByUserId($user->getId());
        //give transaction to the page
        $smarty->assign('debts', $debts);
        //display page
        $smarty->display('loan.tpl');
    }
    else{
        //else redirect to signin page
        header('Location: signin.php'); 
    }
    unset($_SESSION['err']);
    
?>