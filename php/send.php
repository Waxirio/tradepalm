<?php

    require_once('header.php');
    require_once('class/User.php');

    $title = "Send Palm";

    //give title
    $smarty->assign('title', $title);

    //if the connection is true
    if(isset($_SESSION['id'])){

        if(isset($_POST['id'])){
            $user = User::getUserById($_SESSION['id']);
            $smarty->assign('user', $user);
            //receiver
            $userReceiver = User::getUserById($_POST['id']);
            //give user to page
            $smarty->assign('userReceiver', $userReceiver);
            //display page
            $smarty->display('send.tpl');
        }
        else{
            header('Location: userlist.php');
        }
    }
    else{
        //else redirect to signin page
        header('Location: signin.php'); 
    }
    
?>