<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:31:14
  from '/home/waxirio/Documents/mywork/views/market.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e49c2a4f117_11727760',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '4bd535f5ceefa8d74e39ca66b6470d02314e2a88' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/market.tpl',
      1 => 1561066674,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar_connection.tpl' => 1,
  ),
),false)) {
function content_5d0e49c2a4f117_11727760 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar_connection.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('user'=>$_smarty_tpl->tpl_vars['user']->value), 0, false);
?>
        </header>

        <main>
            <div class="container">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <div class="card main-color center">
                            <div class="card-content">
                                <h5>Equivalent Palmier / Papillotes</h5>
                                <div>
                                    <span class="market-value"> 
                                        <?php echo $_smarty_tpl->tpl_vars['lastValue']->value->getPalm();?>

                                    </span>
                                    <span class="market-value"> | <span>
                                    <span class="market-value">
                                        <?php echo $_smarty_tpl->tpl_vars['lastValue']->value->getPap();?>

                                    </span>
                                </div>
                                <div><?php echo $_smarty_tpl->tpl_vars['lastValue']->value->getPalm();?>
 palmier.s vaut <?php echo $_smarty_tpl->tpl_vars['lastValue']->value->getPap();?>
 papillote.s</div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- pour le calcul direct /!\ pas la valeur prise pour la transaction -->
                <input id="cPalm" class="hide" value="<?php echo $_smarty_tpl->tpl_vars['lastValue']->value->getPalm();?>
">
                <input id="cPap" class="hide" value="<?php echo $_smarty_tpl->tpl_vars['lastValue']->value->getPap();?>
">

                <div class="row">
                    <div class="col s12 l6 m6">
                        <div class="card main-color-border">
                            <div class="card-content">
                                <div class="row">
                                    <h5 class="center">Acheter des palmiers</h5>
                                    <form method="post" action="./p_buy.php">
                                        
                                        <button class="right btn main-color" type="submit" name="action">Acheter
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l6 m6">
                        <div class="card main-color-border">
                            <div class="card-content">
                                <div class="row">
                                    <h5 class="center">Vendre des palmiers</h5>
                                    <form method="post" action="./p_sell.php">
                                        <div class="input-field col s12">
                                            <input name="nbPalm" placeholder="Placeholder" id="PalmSellValue" type="text" class="validate">
                                            <label for="nbPalm">Nombre de palmiers</label>
                                        </div>
                                        <span class="main-color-text-bold">Papillotes : </span>
                                        <span id="PapSellValue" class="main-color-text-bold">0</span>
                                        <button class="right btn main-color" type="submit" name="action">Vendre
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <?php if (isset($_smarty_tpl->tpl_vars['trades']->value)) {?>
                    <h5 class="center"> Transactions du marché </h5>
                    <div class="row">
                        <div class="col s12 l12 m12">
                            <div class="card main-color">
                                <div class="card-content">
                                    <div class="row">
                                        <table class="white-text">
                                            <thead>
                                                <tr>
                                                    <th>Type</th>
                                                    <th>Palmiers</th>
                                                    <th>Papillotes</th>
                                                    <th>Effectué le</th>
                                                </tr>
                                            </thead>
                                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trades']->value, 'trade');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['trade']->value) {
?>
                                                <tbody>
                                                    <tr>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['trade']->value->getType();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['trade']->value->getPalm();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['trade']->value->getPap();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['trade']->value->getDate();?>
</td>
                                                    </tr>
                                                </tbody>    
                                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                <?php }?>
            </div>
        </main>
    </body>

    <footer>
        <?php echo '<script'; ?>
 type="text/javascript">
            $('#PalmSellValue').on('keyup', function(){
                $('#PapSellValue').html(
                    String($('#PalmSellValue').val()*$('#cPap').val()/$('#cPalm').val())
                );
            });
        <?php echo '</script'; ?>
>
    </footer>
</html><?php }
}
