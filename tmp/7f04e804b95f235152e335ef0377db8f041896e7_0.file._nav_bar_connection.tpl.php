<?php
/* Smarty version 3.1.33, created on 2019-06-20 22:16:01
  from '/home/waxirio/Documents/mywork/layout/_nav_bar_connection.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0be98130cae5_62086740',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '7f04e804b95f235152e335ef0377db8f041896e7' => 
    array (
      0 => '/home/waxirio/Documents/mywork/layout/_nav_bar_connection.tpl',
      1 => 1561048447,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0be98130cae5_62086740 (Smarty_Internal_Template $_smarty_tpl) {
?><div class="navbar-fixed">
    <nav class="main-color">
        <div class="nav-wrapper">
            <a href="../index.php" class="brand-logo center">Palm Counter</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="./welcome.php">Dashboard</a></li>
                <li><a href="./trade.php">Transactions</a></li>
                <li><a href="./userlist.php">Envoyer</a></li>
                <li><a href="./market.php">Marché</a></li>
            </ul>
            <?php if (isset($_smarty_tpl->tpl_vars['user']->value)) {?>
                <ul class="left hide-on-med-and-down">
                    <li><a href="./p_signout.php">Deconnexion :</a></li>
                    <li><a href="./welcome.php"><?php echo $_smarty_tpl->tpl_vars['user']->value->getLogin();?>
</a></li>
                    <li><a>Palmiers: <?php echo $_smarty_tpl->tpl_vars['user']->value->getPalm();?>
</a></li>
                    <li><a>Papillotes: <?php echo $_smarty_tpl->tpl_vars['user']->value->getPap();?>
</a></li>
                </ul>
                <ul class="left hide-on-large-only">
                    <li><a>Pm: <?php echo $_smarty_tpl->tpl_vars['user']->value->getPalm();?>
</a></li>
                </ul>
                <ul class="right hide-on-large-only">
                    <li><a>Pp: <?php echo $_smarty_tpl->tpl_vars['user']->value->getPap();?>
</a></li>
                </ul>
            <?php }?>
        </div>
    </nav>
</div>

<ul class="sidenav" id="mobile-demo">
    <li><a href="./p_signout.php">Deconnexion</a></li>
    <li><a href="./welcome.php">Dashboard</a></li>
    <li><a href="./trade.php">Transactions</a></li>
    <li><a href="./userlist.php">Envoyer</a></li>
    <li><a href="./market.php">Marché</a></li>
</ul>

<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });
<?php echo '</script'; ?>
><?php }
}
