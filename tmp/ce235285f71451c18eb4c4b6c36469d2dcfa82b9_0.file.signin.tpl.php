<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:29:21
  from '/home/waxirio/Documents/mywork/views/signin.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e49510528b4_96046233',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'ce235285f71451c18eb4c4b6c36469d2dcfa82b9' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/signin.tpl',
      1 => 1560951430,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar.tpl' => 1,
  ),
),false)) {
function content_5d0e49510528b4_96046233 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </header>

        <main>
            <div class="container">
                <?php if (isset($_smarty_tpl->tpl_vars['msg']->value)) {?>
                    <div class="card red-color-border  center">                       
                        <span><?php echo $_smarty_tpl->tpl_vars['msg']->value;?>
</span>               
                    </div>
                <?php }?>
            </div>
            <div class="row hide-on-med-and-down">
                <div class="main-color-text center col s4 l4 m4">
                    <h2>Login</h2>
                    <img class="palm-image center" src="../img/palm.png" alt="marvelous palm" />
                    <div>Gérez vos palmiers en détails où que vous soyez</div>
                </div>
                <div class="col s8 m8 l8 left-align">
                    <div class="card main-color-border log-form">
                        <form action="../php/p_signin.php" method="post">
                            <h3 class="center" >Connexion</h3>
                            <div class="divider"></div>
                            <div class="container main-color-text">
                                <div class="row">
                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">person</i>
                                        <input name="login" id="icon_prefix" type="text" class="validate">
                                        <label for="icon_prefix">Pseudo</label>
                                    </div>

                                    <div class="input-field col s12 m12 l12">
                                        <i class="material-icons prefix">lock</i>
                                        <input name="pass" id="icon_pass" type="password" class="validate">
                                        <label for="icon_pass">Password</label>
                                    </div>
                                
                                    <button class="right btn main-color" type="submit" name="action">
                                        Connexion
                                        <i class="material-icons right">send</i>
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
                <div class="hide-on-large-only">
                    <div class="container">
                        <div class="card main-color-border center">
                            <div class="row center">
                                <form class="col s12 m12 l12" action="../php/p_signin.php" method="post">
                                    <div class="container main-color-text">
                                        <h4>Connectez vous avec votre pseudo</h4>
                                        <div class="divider"></div>
                                        <div class="row">
                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">person</i>
                                                <input name="login" id="icon_prefix" type="text" class="validate">
                                                <label for="icon_prefix">Pseudo</label>
                                            </div>

                                            <div class="input-field col s12 m12 l12">
                                                <i class="material-icons prefix">lock</i>
                                                <input name="pass" id="icon_pass" type="password" class="validate">
                                                <label for="icon_pass">Password</label>
                                            </div>
                                        </div>
                                    
                                        <button class="right btn main-color" type="submit" name="action">
                                            Connexion
                                            <i class="material-icons right">send</i>
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>          
        </main>
    </body>

    <footer>
    </footer>
</html><?php }
}
