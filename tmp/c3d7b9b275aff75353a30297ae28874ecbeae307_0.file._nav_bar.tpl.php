<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:28:58
  from '/home/waxirio/Documents/mywork/layout/_nav_bar.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e493aab64d3_17182628',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'c3d7b9b275aff75353a30297ae28874ecbeae307' => 
    array (
      0 => '/home/waxirio/Documents/mywork/layout/_nav_bar.tpl',
      1 => 1561039803,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
  ),
),false)) {
function content_5d0e493aab64d3_17182628 (Smarty_Internal_Template $_smarty_tpl) {
?><nav class="main-color">
    <div class="nav-wrapper">
        <a href="../index.php" class="brand-logo center">Palm Counter</a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="../index.php">Home</a></li>
            <li><a href="../php/signin.php">Connexion</a></li>
            <li><a href="../php/signup.php">S'inscrire</a></li>
            <li><a href="../php/about.php">A propos</a></li>
        </ul>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    <li><a href="../index.php">Home</a></li>
    <li><a href="../php/signin.php">Connexion</a></li>
    <li><a href="../php/signup.php">S'inscrire</a></li>
    <li><a href="../php/about.php">A propos</a></li>
</ul>

<?php echo '<script'; ?>
>
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });
<?php echo '</script'; ?>
><?php }
}
