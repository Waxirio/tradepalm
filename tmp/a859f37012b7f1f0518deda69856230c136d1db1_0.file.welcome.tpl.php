<?php
/* Smarty version 3.1.33, created on 2019-06-20 22:42:46
  from '/home/waxirio/Documents/mywork/views/welcome.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0befc6c35f85_37047783',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a859f37012b7f1f0518deda69856230c136d1db1' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/welcome.tpl',
      1 => 1561049650,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar_connection.tpl' => 1,
  ),
),false)) {
function content_5d0befc6c35f85_37047783 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar_connection.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('user'=>$_smarty_tpl->tpl_vars['user']->value), 0, false);
?>
        </header>

        <main>
            <!-- Image and title on it -->
            <div class="image-container">
                <img style="width:100%;" src="../img/bandeau.png" alt="bandeau">
                <h4 class="title-on-image center main-color-text hide-on-large-only">Dashboard</h4>
                <h2 class="title-on-image center main-color-text hide-on-med-and-down">Dashboard</h2>
            </div>

            <div class="container">
                <?php if (isset($_smarty_tpl->tpl_vars['err']->value)) {?>
                    <div class="card red-color-border center">                       
                        <span><?php echo $_smarty_tpl->tpl_vars['err']->value;?>
</span>               
                    </div>
                <?php }?>
            </div>

            <div class="container">
                <!-- Row for palms and account informations -->
                <div class="row">
                    <div class="col s12 l6 m12">
                        <div class="info-card card main-color center">
                            <div class="card-content center">
                                <h5 class="center">Info des palmiers</h5>
                                <div>
                                    <span class="title-info"> Nombre de palmiers :</span>
                                    <span class="info"> <?php echo $_smarty_tpl->tpl_vars['user']->value->getPalm();?>
 </span>
                                </div>
                                <div>
                                    <span class="title-info"> Palmiers en cours d'envoi : </span>
                                    <span class="info"> <?php echo $_smarty_tpl->tpl_vars['restPalm']->value;?>
 </span>
                                </div>
                                <div>
                                    <span class="title-info"> Wallet Id : </span>
                                    <span class="info-id"> <?php echo $_smarty_tpl->tpl_vars['user']->value->getWalletId();?>
 </span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col s12 l6 m12">
                        <div class="info-card card main-color center">
                            <div class="card-content center">
                                <h5 class="center">Info du compte</h5>
                                <div>
                                    <span class="title-info"> Pseudo : </span>
                                    <span class="info"> <?php echo $_smarty_tpl->tpl_vars['user']->value->getLogin();?>
 </span>
                                </div>
                                <div>
                                    <span class="title-info"> Email : </span>
                                    <span class="info"> <?php echo $_smarty_tpl->tpl_vars['user']->value->getEmail();?>
 </span>
                                </div>
                                <div>
                                    <span class="title-info"> Compte crée le : </span>
                                    <span class="info"> <?php echo $_smarty_tpl->tpl_vars['user']->value->getCreatedAt();?>
 </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Row title -->
                <div class="row">
                    <div class="col s12 m12 l12">
                        <h4 class="center">- Actions -</h4>
                    </div>
                </div>

                <!-- Row for other information -->
                <div class="row">
                    <div class="col s6 m6 l6">
                        <a href="./trade.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Gestionnaire des envois
                                </div>
                            </div>
                        </a>    
                    </div>
                    <div class="col s6 m6 l6">
                        <a href="./userlist.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Envoyer des palmiers/papillotes
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col s6 m6 l6">
                        <a href="./market.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Aller sur le marché
                                </div>
                            </div>
                        </a>
                    </div>
                    <div class="col s6 m6 l6">
                        <a href="./loan.php">
                            <div class="card main-color">
                                <div class="card-content white-text center">
                                    Faire un prêt
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html><?php }
}
