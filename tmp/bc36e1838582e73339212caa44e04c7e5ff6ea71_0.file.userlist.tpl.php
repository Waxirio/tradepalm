<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:29:55
  from '/home/waxirio/Documents/mywork/views/userlist.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e497375af73_00359347',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'bc36e1838582e73339212caa44e04c7e5ff6ea71' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/userlist.tpl',
      1 => 1561042389,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar_connection.tpl' => 1,
  ),
),false)) {
function content_5d0e497375af73_00359347 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar_connection.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('user'=>$_smarty_tpl->tpl_vars['user']->value), 0, false);
?>
        </header>

        <main>
            <div class="container">
                <?php if (isset($_smarty_tpl->tpl_vars['err']->value)) {?>
                    <div class="card red-color-border center">                       
                        <span><?php echo $_smarty_tpl->tpl_vars['err']->value;?>
</span>               
                    </div>
                <?php }?>
            </div>
            <!-- Search user -->
            <div class="container">
                <h5 class="center">Cherchez un utilisateur par nom ou par wallet Id</h5>
                <nav class="search-zone">
                    <div class="nav-wrapper main-color search-zone">
                        <form action="./userlist.php" method="post">
                            <div class="input-field search-zone">
                                <input name="search" id="search" type="search" required>
                                <label class="label-icon" for="search"><i class="material-icons">search</i></label>
                                <i class="material-icons">close</i>
                            </div>
                        </form>
                    </div>
                </nav>
            </div>

            <?php if (isset($_smarty_tpl->tpl_vars['search']->value)) {?>
                <h6 class="center">Résultat de la recherche pour [<?php echo $_smarty_tpl->tpl_vars['search']->value;?>
]</h6>

                <!-- Search the right one -->
                <?php if (isset($_smarty_tpl->tpl_vars['userLogin']->value) && !empty($_smarty_tpl->tpl_vars['userLogin']->value)) {?>
                    <div class="container">
                        <ul class="collection with-header main-color-negative">
                            <li class="collection-header"><h4>Trouvez la bonne personne (pseudo)</h4></li>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['userLogin']->value, 'u');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['u']->value) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['u']->value != $_smarty_tpl->tpl_vars['user']->value) {?>
                                    <form method="post" action="./send.php">
                                        <input class="hide" name="id" value="<?php echo $_smarty_tpl->tpl_vars['u']->value->getId();?>
">
                                        <li class="collection-item">
                                            <div>
                                                <div class="row">
                                                    <div class="col s6 m6 l6 main-color-bold valign-wrapper">
                                                        <?php echo $_smarty_tpl->tpl_vars['u']->value->getLogin();?>

                                                    </div>
                                                    <div class="col s6 m6 l6 valign-wrapper">
                                                        <?php echo $_smarty_tpl->tpl_vars['u']->value->getWalletId();?>

                                                        <button class="right btn main-color-btn " type="submit" name="action">Envoyer
                                                            <i class="material-icons right">send</i>
                                                        </button>
                                                    </div>
                                                </div>    
                                            </div>
                                        </li>
                                    </form>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>
                    </div>
                <?php }?>

                <?php if (isset($_smarty_tpl->tpl_vars['userWallet']->value) && !empty($_smarty_tpl->tpl_vars['userWallet']->value)) {?>
                    <div class="container">
                        <ul class="collection with-header main-color-negative">
                            <li class="collection-header"><h4>Trouvez la bonne personne (walletId)</h4></li>
                            <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['userWallet']->value, 'u');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['u']->value) {
?>
                                <?php if ($_smarty_tpl->tpl_vars['u']->value != $_smarty_tpl->tpl_vars['user']->value) {?>
                                    <form method="post" action="./send.php">
                                        <input class="hide" name="id" value="<?php echo $_smarty_tpl->tpl_vars['u']->value->getId();?>
">
                                        <li class="collection-item">
                                            <div>
                                                <div class="row">
                                                    <div class="col s6 m6 l6 main-color-bold">
                                                        <?php echo $_smarty_tpl->tpl_vars['u']->value->getLogin();?>

                                                    </div>
                                                    <div class="col s6 m6 l6">
                                                        <?php echo $_smarty_tpl->tpl_vars['u']->value->getWalletId();?>

                                                        <button class="right btn main-color" type="submit" name="action">Envoyer
                                                            <i class="material-icons right">send</i>
                                                        </button>  
                                                    </div>
                                                </div>    
                                            </div>
                                        </li>
                                    </form>
                                <?php }?>
                            <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                        </ul>
                    </div>
                <?php }?>
            <?php }?>            
        </main>
    </body>

    <footer>
    </footer>
</html><?php }
}
