<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:30:18
  from '/home/waxirio/Documents/mywork/views/send.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e498a209259_23759699',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    'a0e877d8150b7b8a10b8e293d09b015ad4531c22' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/send.tpl',
      1 => 1561040964,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar_connection.tpl' => 1,
  ),
),false)) {
function content_5d0e498a209259_23759699 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar_connection.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('user'=>$_smarty_tpl->tpl_vars['user']->value), 0, false);
?>
        </header>

        <main>
            <div class="container">
                <div class="row">
                    <div class="col s12 m12 l12">
                        <h4 class="center">Envoyer des palmier à :</h4>
                        <div class="card main-color-border center">
                            <form action="../php/p_send.php" method="post">
                                <div class="card-content">
                                    <div> 
                                        Pseudo: <span class="main-color-bold truncate"> <?php echo $_smarty_tpl->tpl_vars['userReceiver']->value->getLogin();?>
 </span>
                                    </div>
                                    <div> 
                                        Wallet Id: <span class="main-color-bold truncate"> <?php echo $_smarty_tpl->tpl_vars['userReceiver']->value->getWalletId();?>
 </span>
                                    </div>
                                    <div>
                                        Palmiers: 
                                        <div class="input-field inline main-color-bold">
                                            <input value="0" name="quantitePalm" id="number_give" type="text" class="validate">
                                            <label for="number_give">Quantité de palmiers</label>
                                        </div>
                                    </div>
                                    <div>
                                        Papillotes: 
                                        <div class="input-field inline main-color-bold">
                                            <input value="0" name="quantitePap" id="number_give" type="text" class="validate">
                                            <label for="number_give">Quantité de papillotes</label>
                                        </div>
                                    </div>
                                    <input class="hide" name="id" value="<?php echo $_smarty_tpl->tpl_vars['userReceiver']->value->getId();?>
">
                                    <div>                                    
                                        <button class="btn main-color" type="submit" name="action">
                                            Envoyer les palmiers
                                            <i class="material-icons right">send</i>
                                        </button>        
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html><?php }
}
