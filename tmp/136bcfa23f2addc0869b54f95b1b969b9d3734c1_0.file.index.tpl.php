<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:28:58
  from '/home/waxirio/Documents/mywork/views/index.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e493aaa0763_04719076',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '136bcfa23f2addc0869b54f95b1b969b9d3734c1' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/index.tpl',
      1 => 1561046828,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar.tpl' => 1,
  ),
),false)) {
function content_5d0e493aaa0763_04719076 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
        </header>

        <main class="center">
            <!-- Image and title on it -->
            <div class="image-container">
                <img style="width:100%;" src="../img/bandeau.png" alt="bandeau">
                <h4 class="title-on-image center main-color-text hide-on-large-only">Gestionnaire de Palmiers</h4>
                <h2 class="title-on-image center main-color-text hide-on-med-and-down">Gestionnaire de Palmiers</h2>
                <div class="tips-on-image main-color-text hide-on-med-and-down">
                    Bienvenu sur le compteur de palmiers ! <br>
                    Je vous invite à créer votre compte et ainsi accéder à la gestion de vos échanges en palmiers !
                </div>
            </div>

            <!-- pro tips for beginning -->
            <div class="card main-color">
                <div class="card-content white-text center hide-on-large-only">
                    Bienvenu sur le compteur de palmiers ! <br>
                    Je vous invite à créer votre compte et ainsi accéder à la gestion de vos échanges en palmiers !
                </div>
            </div>

            <!-- Possibility to subscribe or connect -->
            <div class="row">
                <div class="col s6 m6 l6">
                    <a href="../php/about.php">
                        <div class="card main-color hoverable">
                            <div class="card-content white-text center">
                                <div>Qu'es-ce que c'est ?</div>
                            </div>
                        </div>
                    </a>
                    <a href="../php/signup.php">
                        <div class="card main-color hoverable">
                            <div class="card-content white-text center">
                                <div>Inscription</div>
                            </div>
                        </div>
                    </a>
                    <a href="../php/signin.php">
                        <div class="card main-color hoverable">
                            <div class="card-content white-text center">
                                <div>Connexion</div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col s6 m6 l6">
                    <div class="card main-color">
                        <div class="card-content center">
                            <table>
                                <thead class="white-text">
                                    <tr>
                                        <th>Quelques chiffres</th>
                                    </tr>
                                </thead>

                                <tbody class="white-text">
                                    <tr>
                                        <td>Inscrits</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['nbInscrit']->value;?>
</td>
                                    </tr>
                                    <tr>
                                        <td>Echanges effectués</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['nbTrade']->value;?>
</td>
                                    </tr>
                                    <tr>
                                        <td>Palmiers échangés</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['nbPalmTraded']->value;?>
</td>
                                    </tr>
                                    <tr>
                                        <td>Papillotes échangés</td>
                                        <td><?php echo $_smarty_tpl->tpl_vars['nbPalpTraded']->value;?>
</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html><?php }
}
