<?php
/* Smarty version 3.1.33, created on 2019-06-22 17:29:46
  from '/home/waxirio/Documents/mywork/views/trade.tpl' */

/* @var Smarty_Internal_Template $_smarty_tpl */
if ($_smarty_tpl->_decodeProperties($_smarty_tpl, array (
  'version' => '3.1.33',
  'unifunc' => 'content_5d0e496a556a55_22747992',
  'has_nocache_code' => false,
  'file_dependency' => 
  array (
    '58c6fd45b286917d1c2448a470a8b01d574c70dd' => 
    array (
      0 => '/home/waxirio/Documents/mywork/views/trade.tpl',
      1 => 1561042585,
      2 => 'file',
    ),
  ),
  'includes' => 
  array (
    'file:../layout/_base.tpl' => 1,
    'file:../layout/_nav_bar_connection.tpl' => 1,
  ),
),false)) {
function content_5d0e496a556a55_22747992 (Smarty_Internal_Template $_smarty_tpl) {
?><html>

    <head>
        <title><?php echo $_smarty_tpl->tpl_vars['title']->value;?>
</title>
        <?php $_smarty_tpl->_subTemplateRender('file:../layout/_base.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array(), 0, false);
?>
    </head>

    <body>
        <header>
            <!-- Navigation bar -->
            <?php $_smarty_tpl->_subTemplateRender('file:../layout/_nav_bar_connection.tpl', $_smarty_tpl->cache_id, $_smarty_tpl->compile_id, 0, $_smarty_tpl->cache_lifetime, array('user'=>$_smarty_tpl->tpl_vars['user']->value), 0, false);
?>
        </header>

        <main>
            <!-- Error message -->
            <div class="container">
                <?php if (isset($_smarty_tpl->tpl_vars['_SESSION']->value['err'])) {?>
                    <div class="card red-color-border center">                       
                        <span><?php echo $_smarty_tpl->tpl_vars['_SESSION']->value['err'];?>
</span>               
                    </div>
                <?php }?>
            </div>

            <div class="container">
                <!-- trades a valider -->
                <h4 class="center">Transactions a valider</h4>
                <div class="row">
                    <!-- Par une autre personne -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                                <h5 class="center">Débits à valider par le receveur</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Refuser</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trans']->value, 't');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value) {
?>
                                            <!-- si le trade est pas validé -->
                                            <!-- si le trade a pour receveur une autre personne -->
                                            <?php if ($_smarty_tpl->tpl_vars['t']->value->getValidate() == 0 && $_smarty_tpl->tpl_vars['t']->value->getIdR() != $_smarty_tpl->tpl_vars['user']->value->getId()) {?>
                                                <tr>
                                                    <form action="../php/p_finishTrade.php" method="post">
                                                        <input class="hide" value="<?php echo $_smarty_tpl->tpl_vars['t']->value->getId();?>
" name="id">
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameG();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameR();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPalm();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPap();?>
</td>
                                                        <td>
                                                            <button class="btn main-color-negative" value="cancel" type="submit" name="cancel">
                                                                Refuser
                                                            </button>
                                                        </td>
                                                    </form>
                                                </tr>
                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- Par soi -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                            <h5 class="center">Crédits à valider</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Refuser</th>
                                            <th>Valider</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trans']->value, 't');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value) {
?>
                                            <!-- si le trade est pas validé -->
                                            <!-- si le trade a pour receveur le user connecté -->
                                            <?php if ($_smarty_tpl->tpl_vars['t']->value->getValidate() == 0 && $_smarty_tpl->tpl_vars['t']->value->getIdR() == $_smarty_tpl->tpl_vars['user']->value->getId()) {?>
                                                <tr>
                                                    <form action="../php/p_finishTrade.php" method="post">
                                                        <input class="hide" value="<?php echo $_smarty_tpl->tpl_vars['t']->value->getId();?>
" name="id">
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameG();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameR();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPalm();?>
</td>
                                                        <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPap();?>
</td>
                                                        <td>
                                                            <button class="btn main-color-negative" value="cancel" type="submit" name="cancel">
                                                                Refuser
                                                            </button>
                                                        </td>
                                                        <td>
                                                            <button class="btn main-color-negative" value="accept" type="submit" name="accept">
                                                                Valider
                                                            </button>
                                                        </td>
                                                    </form>
                                                </tr>
                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
                
                <h4 class="center">Transactions validées</h4>
                <div class="row">
                    <!-- trades validés donneur -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                            <h5 class="center">Vos débits</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Emis le</th>
                                            <th>Validé le</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trans']->value, 't');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value) {
?>
                                            <!-- si le trade est validé -->
                                            <!-- si le trade a pour donneur le user connecté -->
                                            <?php if ($_smarty_tpl->tpl_vars['t']->value->getValidate() == 1 && $_smarty_tpl->tpl_vars['t']->value->getIdG() == $_smarty_tpl->tpl_vars['user']->value->getId()) {?>
                                                <tr>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameG();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameR();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPalm();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPap();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getCreatedAt();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getValidateAt();?>
</td>
                                                </tr>
                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <!-- trades validés receveur -->
                    <div class="col s12 m12 l12">
                        <div class="card main-color">
                            <div class="card-content">
                            <h5 class="center">Vos crédits</h5>
                                <table class="main-color">
                                    <thead>
                                        <tr>
                                            <th>Envoyeur</th>
                                            <th>Receveur</th>
                                            <th>Palmiers</th>
                                            <th>Papillotes</th>
                                            <th>Emis le</th>
                                            <th>Validé le</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
$_from = $_smarty_tpl->smarty->ext->_foreach->init($_smarty_tpl, $_smarty_tpl->tpl_vars['trans']->value, 't');
if ($_from !== null) {
foreach ($_from as $_smarty_tpl->tpl_vars['t']->value) {
?>
                                            <!-- si le trade est validé -->
                                            <!-- si le trade a pour receveur le user connecté -->
                                            <?php if ($_smarty_tpl->tpl_vars['t']->value->getValidate() == 1 && $_smarty_tpl->tpl_vars['t']->value->getIdR() == $_smarty_tpl->tpl_vars['user']->value->getId()) {?>
                                                <tr>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameG();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getNameR();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPalm();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getAmountPap();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getCreatedAt();?>
</td>
                                                    <td><?php echo $_smarty_tpl->tpl_vars['t']->value->getValidateAt();?>
</td>
                                                </tr>
                                            <?php }?>
                                        <?php
}
}
$_smarty_tpl->smarty->ext->_foreach->restore($_smarty_tpl, 1);?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </body>

    <footer>
    </footer>
</html><?php }
}
