<?php

    require_once('header.php');
    require_once('php/class/User.php');
    require_once('php/class/Trade.php');

    $title = "PalmCounter";

    //give title
    $smarty->assign('title', $title);

    //Display smarty page
    if(isset($_SESSION['id'])){
        header('Location: ./php/welcome.php'); 
    }
    else{
        $inscrits = User::getNbInscrits();
        $smarty->assign('nbInscrit', $inscrits);
        $smarty->assign('nbTrade',Trade::getNbTrades());
        $smarty->assign('nbPalmTraded',Trade::getNbPalmTraded());
        $smarty->assign('nbPalpTraded',Trade::getNbPapTraded());
        $smarty->display('index.tpl');
    }
?>