<?php
    //Smarty template engine
    session_start();
    //Include smarty object
    require_once('libs/Smarty.class.php');

    //Create new smarty object
    $smarty = new Smarty();

    //Set working folders
    $smarty->template_dir = 'views';
    $smarty->compile_dir = 'tmp';
?>