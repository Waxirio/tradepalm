CREATE TABLE users
(
    id INT NOT NULL AUTO_INCREMENT,
    wallet_id VARCHAR(256),
    pseudo VARCHAR(128),
    email VARCHAR(128),
    pass VARCHAR(256),
    palm INT,
    created_at DATE,
    last_connection DATE,
    PRIMARY KEY (id)
);

CREATE TABLE transactions
(
    id INT NOT NULL AUTO_INCREMENT,
    id_giver INT NOT NULL,
    id_receiver INT NOT NULL,
    amount INT NOT NULL,
    validate BOOLEAN NOT NULL default 0,
    created_at DATE,
    validate_at DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (id_giver) REFERENCES users(id),
    FOREIGN KEY (id_receiver) REFERENCES users(id)
);

CREATE TABLE palmDebt
(
    id INT NOT NULL AUTO_INCREMENT,
    id_user INT NOT NULL,
    amount INT NOT NULL,
    debt INT NOT NULL,
    created_at DATE,
    finished_at DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES users(id)
);

/*********************************************************************************************/


CREATE TABLE users
(
    id INT NOT NULL AUTO_INCREMENT,
    wallet_id VARCHAR(256),
    referral_id VARCHAR(256),
    pseudo VARCHAR(128),
    email VARCHAR(128),
    pass VARCHAR(256),
    palmiers INT,
    papillotes INT,
    created_at DATE,
    last_connection DATE,
    PRIMARY KEY (id)
);

CREATE TABLE transactions
(
    id INT NOT NULL AUTO_INCREMENT,
    id_giver INT NOT NULL,
    id_receiver INT NOT NULL,
    amount_palm INT NOT NULL,
    amount_pap INT NOT NULL,
    validate BOOLEAN NOT NULL default 0,
    created_at DATE,
    validate_at DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (id_giver) REFERENCES users(id),
    FOREIGN KEY (id_receiver) REFERENCES users(id)
);

CREATE TABLE debt
(
    id INT NOT NULL AUTO_INCREMENT,
    id_user INT NOT NULL,
    debt_palm INT NOT NULL,
    debt_pap INT NOT NULL,
    pay_palm INT NOT NULL,
    pay_pap INT NOT NULL,
    created_at DATE,
    finished_at DATE,
    PRIMARY KEY (id),
    FOREIGN KEY (id_user) REFERENCES users(id)
);

CREATE TABLE market
(
    id INT NOT NULL AUTO_INCREMENT,
    nb_palm INT NOT NULL,
    nb_pap INT NOT NULL,
    created_at DATE NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE market_trade
(
    id INT NOT NULL AUTO_INCREMENT,
    id_user INT NOT NULL,
    nb_palm INT NOT NULL,
    nb_pap INT NOT NULL,
    did_at DATE NOT NULL,
    type VARCHAR(32) NOT NULL,
    PRIMARY KEY (id)
);