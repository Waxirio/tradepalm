<nav class="main-color">
    <div class="nav-wrapper">
        <a href="../index.php" class="brand-logo center">Palm Counter</a>
        <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
        <ul class="right hide-on-med-and-down">
            <li><a href="../index.php">Home</a></li>
            <li><a href="../php/signin.php">Connexion</a></li>
            <li><a href="../php/signup.php">S'inscrire</a></li>
            <li><a href="../php/about.php">A propos</a></li>
        </ul>
    </div>
</nav>

<ul class="sidenav" id="mobile-demo">
    <li><a href="../index.php">Home</a></li>
    <li><a href="../php/signin.php">Connexion</a></li>
    <li><a href="../php/signup.php">S'inscrire</a></li>
    <li><a href="../php/about.php">A propos</a></li>
</ul>

<script>
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });
</script>