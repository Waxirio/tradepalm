<div class="navbar-fixed">
    <nav class="main-color">
        <div class="nav-wrapper">
            <a href="../index.php" class="brand-logo center">Palm Counter</a>
            <a href="#" data-target="mobile-demo" class="sidenav-trigger"><i class="material-icons">menu</i></a>
            <ul class="right hide-on-med-and-down">
                <li><a href="./welcome.php">Dashboard</a></li>
                <li><a href="./trade.php">Transactions</a></li>
                <li><a href="./userlist.php">Envoyer</a></li>
                <li><a href="./market.php">Marché</a></li>
            </ul>
            {if isset($user)}
                <ul class="left hide-on-med-and-down">
                    <li><a href="./p_signout.php">Deconnexion :</a></li>
                    <li><a href="./welcome.php">{$user->getLogin()}</a></li>
                    <li><a>Palmiers: {$user->getPalm()}</a></li>
                    <li><a>Papillotes: {$user->getPap()}</a></li>
                </ul>
                <ul class="left hide-on-large-only">
                    <li><a>Pm: {$user->getPalm()}</a></li>
                </ul>
                <ul class="right hide-on-large-only">
                    <li><a>Pp: {$user->getPap()}</a></li>
                </ul>
            {/if}
        </div>
    </nav>
</div>

<ul class="sidenav" id="mobile-demo">
    <li><a href="./p_signout.php">Deconnexion</a></li>
    <li><a href="./welcome.php">Dashboard</a></li>
    <li><a href="./trade.php">Transactions</a></li>
    <li><a href="./userlist.php">Envoyer</a></li>
    <li><a href="./market.php">Marché</a></li>
</ul>

<script>
    $(document).ready(function(){
        $('.sidenav').sidenav();
    });
</script>